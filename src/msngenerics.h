/*
 * msngenerics.h
 *
 * General MSN protocol schemas and semantics
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifndef MSNGENERICS_H
#define MSNGENERICS_H

#define DEFAULT_NS_PORT 1863
#define DEFAULT_SB_PORT 1863

/* Different user statuses. */
typedef enum {
	ONLINE_ERR,	/* Used to catch errors */
	ONLINE_NLN,	/* Online */
	ONLINE_IDL,	/* Idle */
	ONLINE_AWY,	/* Away */
	ONLINE_BSY,	/* Busy */
	ONLINE_BRB,	/* Be Right Back */
	ONLINE_PHN,	/* On The Phone */
	ONLINE_LUN,	/* Lunch */
	ONLINE_HDN,	/* Appear Offline (only allowed for the local user) */
	ONLINE_FLN	/* Offline */
} OnlineState;

OnlineState msngenerics_decodestatus(const char *status);

#endif	/* MSNGENERICS_H */
