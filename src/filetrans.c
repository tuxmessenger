/*
 * filetrans.c
 *
 * File transfer
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>

#include "debug.h"
#include "sbsessions.h"
#include "msnp2p.h"

/* Offer a file to a single recipient */
static void filetrans_offer(const char *filename, const char *username) {
	
	SbSession *session;

	debug_print("FT: Offering '%s' to '%s'\n", filename, username);

	/* Find a suitable session. */
	session = sbsessions_find_single_safe(username);

	msnp2p_offerfile(session, username, filename);

}

/* Offer a file to multiple recipients */
void filetrans_offer_multiple(const char *filename, char *usernames[], unsigned int num_users) {

	unsigned int i;
	for ( i=0; i<num_users; i++ ) {
		filetrans_offer(filename, usernames[i]);
	}

}
