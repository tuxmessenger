/*
 * routines.h
 *
 * Random Useful Routines and Function
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifndef ROUTINES_H
#define ROUTINES_H

#include <gdk/gdk.h>

extern char *routines_lindex(char *string, unsigned int pos);
extern char *routines_lindexend(char *string, unsigned int pos);

extern char *routines_urldecode(const char *words);
extern char *routines_urlencode(const char *words);

extern char *routines_hostname(const char *input);
extern int routines_port(const char *input);

extern char *routines_base64(char *base64_input);
extern char *routines_base64givenlength(char *base64_input, ssize_t length);
extern size_t routines_base64decode(const char *base64_input, char **output);

extern char *routines_guid(void);

extern char *routines_glob(const char *filename);

extern char *routines_killtriangles(const char *input);
extern char *routines_killtriangles_and_ampersands(const char *input);

extern char *routines_flipcolour(const char *colour);
extern char *routines_gdk_to_hashrgb(const GdkColor *colour);

#endif	/* ROUTINES_H */
