/*
 * prefswindow.c
 *
 * The preferences window
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "options.h"
#include "msnprotocol.h"
#include "debug.h"
#include "contactlist.h"
#include "routines.h"
#include "twnauth.h"
#include "error.h"

/* This is where I go slightly mad.  Solid UI code... */

/* Linked list item definition for storing changes to be made to the block/allow lists */
typedef struct allowblockchange_t {
	struct allowblockchange *next;
	char *string;
} AllowBlockChange;

static struct {

	unsigned int open;			/* Non-zero means the prefs window is open. */
	GtkWidget *window;			/* The prefs window itself */

	GtkWidget *blocklist;			/* vbox in the "Allow/Block lists" tab */
	GtkWidget *reverselist;			/* vbox in the "Reverse list" tab */
	GtkWidget *blocklist_sub;		/* vbox in the "Allow/Block lists" tab */
	GtkWidget *reverselist_sub;		/* vbox in the "Reverse list" tab */
	
	GtkWidget *blocklist_allowbutton;	/* "<- Allow" button */
	GtkWidget *blocklist_blockbutton;	/* "Block ->" button */
	GtkWidget *blocklist_allowclist;	/* CList for AL */
	GtkWidget *blocklist_blockclist;	/* CList for BL */
	GtkWidget *reverselist_clist;		/* CList for RL */
	GtkWidget *reverselist_gtc;		/* Tick box */
	GtkWidget *blocklist_blp;		/* Tick box */
	GtkWidget *ofontbutton;			/* Font button for other contacts */
	GtkWidget *ocolourbutton;		/* Colour button for other contacts */
	
	int block_selected;
	int allow_selected;
	
	GtkWidget *wget;			/* "Wget" box */
	
} prefswindow = {

	0,
	NULL,
	
	NULL,
	NULL,
	NULL,
	NULL,
	
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	
	-1,
	-1,
	
	NULL

};

static void prefswindow_allowselect(GtkWidget *widget, gint row, gint column, GdkEventButton *event, gpointer data) {

	prefswindow.allow_selected = row;
	gtk_widget_set_sensitive(prefswindow.blocklist_blockbutton, 1);
	if ( prefswindow.block_selected != -1 ) {
		gtk_clist_unselect_row(GTK_CLIST(prefswindow.blocklist_blockclist), prefswindow.block_selected, 0);
	}

}

static void prefswindow_allowunselect(GtkWidget *widget, gint row, gint column, GdkEventButton *event, gpointer data) {

	prefswindow.allow_selected = -1;
	gtk_widget_set_sensitive(prefswindow.blocklist_blockbutton, 0);

}

static void prefswindow_blockselect(GtkWidget *widget, gint row, gint column, GdkEventButton *event, gpointer data) {

	prefswindow.block_selected = row;
	gtk_widget_set_sensitive(prefswindow.blocklist_allowbutton, 1);
	if ( prefswindow.allow_selected != -1 ) {
		gtk_clist_unselect_row(GTK_CLIST(prefswindow.blocklist_allowclist), prefswindow.allow_selected, 0);
	}

}

static void prefswindow_blockunselect(GtkWidget *widget, gint row, gint column, GdkEventButton *event, gpointer data) {

	prefswindow.block_selected = -1;
	gtk_widget_set_sensitive(prefswindow.blocklist_allowbutton, 0);

}

static void prefswindow_allowclick() {
}

static void prefswindow_blockclick() {
}

static void prefswindow_filllist(char *list, GtkWidget *clist) {

/*	ContactItem *token;
	char *contact;
	
	token = NULL;
	contact = contactlist_getcontact(list, &token);
	while ( token ) {
		
		char *friendlyname;
		char *friendlyname_decoded;
		char *username;
		char *full_string;
		char *clist_add[1];
		
		username = routines_lindex(contact, 0);
		friendlyname = routines_lindex(contact, 1);
		
		if ( strlen(friendlyname) == 0 ) {
		
			clist_add[0] = username;
		
		} else {

			friendlyname_decoded = routines_urldecode(friendlyname);
			free(friendlyname);
			full_string = malloc(strlen(username) + strlen(friendlyname_decoded) + 3 + 1);
			strcpy(full_string, username);
			strcat(full_string, " - ");
			strcat(full_string, friendlyname_decoded);
			
			clist_add[0] = full_string;

			free(friendlyname_decoded);
			free(username);

		}
		
		gtk_clist_append(GTK_CLIST(clist), clist_add);
		free(clist_add[0]);
		
		contact = contactlist_getcontact(list, &token);
		
	}*/

}

static void prefswindow_fillreverselist() {

/*	ContactItem *token;
	char *contact;
	
	token = NULL;
	contact = contactlist_getcontact("RL", &token);
	while ( token ) {
		
		char *friendlyname;
		char *friendlyname_decoded;
		char *username;
		char *clist_add[3];
		
		username = routines_lindex(contact, 0);
		friendlyname = routines_lindex(contact, 1);

		clist_add[0] = username;
		friendlyname_decoded = routines_urldecode(friendlyname);
		free(friendlyname);
		clist_add[1] = friendlyname_decoded;

		if ( contactlist_isonlist("AL", username) ) {
			clist_add[2] = "Allow";
		} else if ( contactlist_isonlist("BL", username) ) {
			clist_add[2] = "Block";
		} else {
			clist_add[2] = "Neither";
		}		
		
		gtk_clist_append(GTK_CLIST(prefswindow.reverselist_clist), clist_add);
		
		free(friendlyname_decoded);
		free(username);

		contact = contactlist_getcontact("RL", &token);
		
	}*/

}

/* Parallel of mainwindow_setonline. */
void prefswindow_setonline() {

	GtkWidget *prefs_blocklist_vbox;
	GtkWidget *prefs_blocklist_buttonbox;
	GtkWidget *prefs_blocklist_scroll;
	GtkWidget *prefs_blocklist_viewport;
	GtkWidget *prefs_blocklist_hbox;
	GtkWidget *prefs_blocklist_allow_clist_label;
	GtkWidget *prefs_blocklist_block_clist_label;

	GtkWidget *prefs_reverselist_vbox;
	GtkWidget *prefs_reverselist_scroll;
	GtkWidget *prefs_reverselist_clist_username;
	GtkWidget *prefs_reverselist_clist_displayname;
	GtkWidget *prefs_reverselist_clist_status;
	
	if ( !prefswindow.open ) {
		return;
	}
	
	if ( prefswindow.blocklist_sub != NULL ) {
		gtk_widget_destroy(prefswindow.blocklist_sub);
	}
		
	if ( prefswindow.reverselist_sub != NULL ) {
		gtk_widget_destroy(prefswindow.reverselist_sub);
	}
	
	prefswindow.blocklist_sub = gtk_vbox_new(TRUE, 0);
	prefswindow.reverselist_sub = gtk_vbox_new(TRUE, 0);
	gtk_widget_show(prefswindow.reverselist_sub);	
		
	prefs_blocklist_vbox = gtk_vbox_new (FALSE, 0);
	gtk_widget_ref (prefs_blocklist_vbox);
	gtk_object_set_data_full (GTK_OBJECT (prefswindow.blocklist), "prefs_blocklist_vbox", prefs_blocklist_vbox,
					(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (prefs_blocklist_vbox);
	gtk_container_add (GTK_CONTAINER (prefswindow.blocklist_sub), prefs_blocklist_vbox);
	gtk_container_add (GTK_CONTAINER (prefswindow.blocklist), prefswindow.blocklist_sub);
	gtk_widget_show(prefswindow.blocklist_sub);

	prefs_blocklist_scroll = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_ref (prefs_blocklist_scroll);
	gtk_object_set_data_full (GTK_OBJECT (prefswindow.blocklist), "prefs_blocklist_scroll", prefs_blocklist_scroll,
					(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (prefs_blocklist_scroll);
	gtk_box_pack_start (GTK_BOX (prefs_blocklist_vbox), prefs_blocklist_scroll, TRUE, TRUE, 0);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (prefs_blocklist_scroll), GTK_POLICY_NEVER, GTK_POLICY_ALWAYS);

	prefs_blocklist_viewport = gtk_viewport_new (NULL, NULL);
	gtk_widget_ref (prefs_blocklist_viewport);
	gtk_object_set_data_full (GTK_OBJECT (prefswindow.blocklist), "prefs_blocklist_viewport", prefs_blocklist_viewport,
					(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (prefs_blocklist_viewport);
	gtk_container_add (GTK_CONTAINER (prefs_blocklist_scroll), prefs_blocklist_viewport);

	prefs_blocklist_hbox = gtk_hbox_new (FALSE, 0);
	gtk_widget_ref (prefs_blocklist_hbox);
	gtk_object_set_data_full (GTK_OBJECT (prefswindow.blocklist), "prefs_blocklist_hbox", prefs_blocklist_hbox,
					(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (prefs_blocklist_hbox);
	gtk_container_add (GTK_CONTAINER (prefs_blocklist_viewport), prefs_blocklist_hbox);

	prefswindow.blocklist_allowclist = gtk_clist_new (1);
	gtk_widget_ref (prefswindow.blocklist_allowclist);
	gtk_object_set_data_full (GTK_OBJECT (prefswindow.blocklist), "prefs_blocklist_allow_clist", prefswindow.blocklist_allowclist,
					(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (prefswindow.blocklist_allowclist);
	gtk_box_pack_start (GTK_BOX (prefs_blocklist_hbox), prefswindow.blocklist_allowclist, TRUE, TRUE, 0);
	gtk_clist_set_column_width (GTK_CLIST (prefswindow.blocklist_allowclist), 0, 80);
	gtk_clist_column_titles_show (GTK_CLIST (prefswindow.blocklist_allowclist));

	prefs_blocklist_allow_clist_label = gtk_label_new ("Allow");
	gtk_widget_ref (prefs_blocklist_allow_clist_label);
	gtk_object_set_data_full (GTK_OBJECT (prefswindow.blocklist), "prefs_blocklist_allow_clist_label", prefs_blocklist_allow_clist_label,
					(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (prefs_blocklist_allow_clist_label);
	gtk_clist_set_column_widget (GTK_CLIST (prefswindow.blocklist_allowclist), 0, prefs_blocklist_allow_clist_label);

	prefswindow.blocklist_blockclist = gtk_clist_new (1);
	gtk_widget_ref (prefswindow.blocklist_blockclist);
	gtk_object_set_data_full (GTK_OBJECT (prefswindow.blocklist), "prefs_blocklist_block_clist", prefswindow.blocklist_blockclist,
					(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (prefswindow.blocklist_blockclist);
	gtk_box_pack_start (GTK_BOX (prefs_blocklist_hbox), prefswindow.blocklist_blockclist, TRUE, TRUE, 0);
	gtk_clist_set_column_width (GTK_CLIST (prefswindow.blocklist_blockclist), 0, 80);
	gtk_clist_column_titles_show (GTK_CLIST (prefswindow.blocklist_blockclist));
	
	prefs_blocklist_block_clist_label = gtk_label_new ("Block");
	gtk_widget_ref (prefs_blocklist_block_clist_label);
	gtk_object_set_data_full (GTK_OBJECT (prefswindow.blocklist), "prefs_blocklist_block_clist_label", prefs_blocklist_block_clist_label,
					(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (prefs_blocklist_block_clist_label);
	gtk_clist_set_column_widget (GTK_CLIST (prefswindow.blocklist_blockclist), 0, prefs_blocklist_block_clist_label);

	prefs_blocklist_buttonbox = gtk_hbutton_box_new ();
	gtk_widget_ref (prefs_blocklist_buttonbox);
	gtk_object_set_data_full (GTK_OBJECT (prefswindow.blocklist), "prefs_blocklist_buttonbox", prefs_blocklist_buttonbox,
					(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (prefs_blocklist_buttonbox);
	gtk_box_pack_start (GTK_BOX (prefs_blocklist_vbox), prefs_blocklist_buttonbox, FALSE, FALSE, 0);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (prefs_blocklist_buttonbox), GTK_BUTTONBOX_SPREAD);

	prefswindow.blocklist_allowbutton = gtk_button_new_with_label ("<- Allow");
	gtk_widget_ref (prefswindow.blocklist_allowbutton);
	gtk_object_set_data_full (GTK_OBJECT (prefswindow.blocklist), "prefs_blocklist_allow_button", prefswindow.blocklist_allowbutton,
					(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (prefswindow.blocklist_allowbutton);
	gtk_container_add (GTK_CONTAINER (prefs_blocklist_buttonbox), prefswindow.blocklist_allowbutton);
	GTK_WIDGET_SET_FLAGS (prefswindow.blocklist_allowbutton, GTK_CAN_DEFAULT);

	prefswindow.blocklist_blockbutton = gtk_button_new_with_label ("Block ->");
	gtk_widget_ref (prefswindow.blocklist_blockbutton);
	gtk_object_set_data_full (GTK_OBJECT (prefswindow.blocklist), "prefs_blocklist_block_button", prefswindow.blocklist_blockbutton,
					(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (prefswindow.blocklist_blockbutton);
	gtk_container_add (GTK_CONTAINER (prefs_blocklist_buttonbox), prefswindow.blocklist_blockbutton);
	GTK_WIDGET_SET_FLAGS (prefswindow.blocklist_blockbutton, GTK_CAN_DEFAULT);

	gtk_container_set_border_width(GTK_CONTAINER(prefswindow.blocklist), 10);
	gtk_clist_column_titles_passive(GTK_CLIST(prefswindow.blocklist_allowclist));
	gtk_clist_column_titles_passive(GTK_CLIST(prefswindow.blocklist_blockclist));
	
	prefswindow.blocklist_blp = gtk_check_button_new_with_label ("Block all other users");	
	gtk_widget_ref (prefswindow.blocklist_blp);
	gtk_widget_show (prefswindow.blocklist_blp);
	gtk_box_pack_start (GTK_BOX (prefs_blocklist_vbox), prefswindow.blocklist_blp, FALSE, FALSE, 0);
	
	if ( strcmp(options_blp(), "BL") == 0) {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(prefswindow.blocklist_blp), TRUE);
	} else {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(prefswindow.blocklist_blp), FALSE);
	}

	gtk_signal_connect(GTK_OBJECT(prefswindow.blocklist_allowclist), "select_row", GTK_SIGNAL_FUNC(prefswindow_allowselect), NULL);
	gtk_signal_connect(GTK_OBJECT(prefswindow.blocklist_allowclist), "unselect_row", GTK_SIGNAL_FUNC(prefswindow_allowunselect), NULL);
	gtk_signal_connect(GTK_OBJECT(prefswindow.blocklist_blockbutton), "clicked", GTK_SIGNAL_FUNC(prefswindow_allowclick), NULL);

	gtk_signal_connect(GTK_OBJECT(prefswindow.blocklist_blockclist), "select_row", GTK_SIGNAL_FUNC(prefswindow_blockselect), NULL);
	gtk_signal_connect(GTK_OBJECT(prefswindow.blocklist_blockclist), "unselect_row", GTK_SIGNAL_FUNC(prefswindow_blockunselect), NULL);
	gtk_signal_connect(GTK_OBJECT(prefswindow.blocklist_allowbutton), "clicked", GTK_SIGNAL_FUNC(prefswindow_blockclick), NULL);

	prefswindow_filllist("BL", prefswindow.blocklist_blockclist);
	prefswindow_filllist("AL", prefswindow.blocklist_allowclist);

	gtk_widget_set_sensitive(prefswindow.blocklist_allowbutton , 0);
	gtk_widget_set_sensitive(prefswindow.blocklist_blockbutton, 0);
	
	/* ************* now the Reverse List section ************** */
	
	prefs_reverselist_vbox = gtk_vbox_new (FALSE, 3);
	gtk_widget_ref (prefs_reverselist_vbox);
	gtk_object_set_data_full (GTK_OBJECT (prefswindow.reverselist), "prefs_reverselist_vbox", prefs_reverselist_vbox,
			(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (prefs_reverselist_vbox);
	gtk_container_add (GTK_CONTAINER (prefswindow.reverselist_sub), prefs_reverselist_vbox);
	gtk_container_add(GTK_CONTAINER(prefswindow.reverselist), prefswindow.reverselist_sub);

	prefs_reverselist_scroll = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_ref (prefs_reverselist_scroll);
	gtk_object_set_data_full (GTK_OBJECT (prefswindow.reverselist), "prefs_reverselist_scroll", prefs_reverselist_scroll,
				(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (prefs_reverselist_scroll);
	gtk_box_pack_start (GTK_BOX (prefs_reverselist_vbox), prefs_reverselist_scroll, TRUE, TRUE, 0);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (prefs_reverselist_scroll), GTK_POLICY_NEVER, GTK_POLICY_ALWAYS);

	prefswindow.reverselist_clist = gtk_clist_new (3);
	gtk_widget_ref (prefswindow.reverselist_clist);
	gtk_object_set_data_full (GTK_OBJECT (prefswindow.reverselist), "prefs_reverselist_clist", prefswindow.reverselist_clist,
				(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (prefswindow.reverselist_clist);
	gtk_container_add (GTK_CONTAINER (prefs_reverselist_scroll), prefswindow.reverselist_clist);
	gtk_clist_set_column_width (GTK_CLIST (prefswindow.reverselist_clist), 0, 80);
	gtk_clist_set_column_width (GTK_CLIST (prefswindow.reverselist_clist), 1, 80);
	gtk_clist_set_column_width (GTK_CLIST (prefswindow.reverselist_clist), 2, 80);
	gtk_clist_column_titles_show (GTK_CLIST (prefswindow.reverselist_clist));

	prefs_reverselist_clist_username = gtk_label_new ("Username");
	gtk_widget_ref (prefs_reverselist_clist_username);
	gtk_object_set_data_full (GTK_OBJECT (prefswindow.reverselist), "prefs_reverselist_clist_username", prefs_reverselist_clist_username,
				(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (prefs_reverselist_clist_username);
	gtk_clist_set_column_widget (GTK_CLIST (prefswindow.reverselist_clist), 0, prefs_reverselist_clist_username);

	prefs_reverselist_clist_displayname = gtk_label_new ("Display Name");
	gtk_widget_ref (prefs_reverselist_clist_displayname);
	gtk_object_set_data_full (GTK_OBJECT (prefswindow.reverselist), "prefs_reverselist_clist_displayname", prefs_reverselist_clist_displayname,
				(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (prefs_reverselist_clist_displayname);
	gtk_clist_set_column_widget (GTK_CLIST (prefswindow.reverselist_clist), 1, prefs_reverselist_clist_displayname);
	gtk_widget_set_usize (prefs_reverselist_clist_displayname, 128, -2);

	prefs_reverselist_clist_status = gtk_label_new ("Status");
	gtk_widget_ref (prefs_reverselist_clist_status);
	gtk_object_set_data_full (GTK_OBJECT (prefswindow.reverselist), "prefs_reverselist_clist_status", prefs_reverselist_clist_status,
				(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (prefs_reverselist_clist_status);
	gtk_clist_set_column_widget (GTK_CLIST (prefswindow.reverselist_clist), 2, prefs_reverselist_clist_status);
	gtk_widget_set_usize (prefs_reverselist_clist_status, 31, -2);

	prefswindow.reverselist_gtc = gtk_check_button_new_with_label ("Add new users to the Allow List automatically");	
	gtk_widget_ref (prefswindow.reverselist_gtc);
	gtk_object_set_data_full (GTK_OBJECT (prefswindow.reverselist), "prefs_reverselist_gtc", prefswindow.reverselist_gtc,
				(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (prefswindow.reverselist_gtc);
	gtk_box_pack_start (GTK_BOX (prefs_reverselist_vbox), prefswindow.reverselist_gtc, FALSE, FALSE, 0);

	if ( strcmp(options_gtc(), "N") == 0) {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(prefswindow.reverselist_gtc), TRUE);
	} else {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(prefswindow.reverselist_gtc), FALSE);
	}

	gtk_container_set_border_width(GTK_CONTAINER(prefswindow.reverselist), 10);
	gtk_clist_column_titles_passive(GTK_CLIST(prefswindow.reverselist_clist));

	gtk_clist_set_column_width (GTK_CLIST (prefswindow.reverselist_clist), 0, 149);
	gtk_clist_set_column_width (GTK_CLIST (prefswindow.reverselist_clist), 1, 210);
	gtk_clist_set_column_width (GTK_CLIST (prefswindow.reverselist_clist), 2, 69);
	
	prefswindow_fillreverselist();

}

/* Parallel of mainwindow_setdispatch.  Tell the user they can't modify lists while disconnected. */
void prefswindow_setdispatch() {

	GtkWidget *message_label;
	
	if ( !prefswindow.open ) {
		return;
	}
	
	if ( prefswindow.blocklist_sub != NULL ) {
		gtk_widget_destroy(prefswindow.blocklist_sub);
	}
		
	if ( prefswindow.reverselist_sub != NULL ) {
		gtk_widget_destroy(prefswindow.reverselist_sub);
	}
		
	prefswindow.blocklist_sub = gtk_vbox_new(TRUE, 0);
	assert(prefswindow.blocklist_sub != NULL);
	gtk_container_add(GTK_CONTAINER(prefswindow.blocklist), prefswindow.blocklist_sub);
	message_label = gtk_label_new("You must be signed in to view or modify your block list");
	assert(message_label != NULL);
	gtk_box_pack_start(GTK_BOX(prefswindow.blocklist_sub), message_label, TRUE, TRUE, 0);
	gtk_label_set_line_wrap(GTK_LABEL(message_label), TRUE);
	gtk_label_set_justify(GTK_LABEL(message_label), GTK_JUSTIFY_CENTER);
	gtk_widget_show_all(prefswindow.blocklist_sub);

	prefswindow.reverselist_sub = gtk_vbox_new(TRUE, 0);
	assert(prefswindow.reverselist_sub != NULL);
	gtk_container_add(GTK_CONTAINER(prefswindow.reverselist), prefswindow.reverselist_sub);
	message_label = gtk_label_new("You must be signed in to view your reverse list");
	assert(message_label != NULL);
	gtk_box_pack_start(GTK_BOX(prefswindow.reverselist_sub), message_label, TRUE, TRUE, 0);
	gtk_label_set_line_wrap(GTK_LABEL(message_label), TRUE);
	gtk_label_set_justify(GTK_LABEL(message_label), GTK_JUSTIFY_CENTER);
	gtk_widget_show_all(prefswindow.reverselist_sub);

}

static void prefswindow_closed() {
	prefswindow.open = 0;
}

static void prefswindow_lcolsel(GtkWidget *widget, gpointer data) {

	GdkColor colour;
	
	gtk_color_button_get_color(GTK_COLOR_BUTTON(widget), &colour);
	options_setlocalcolour_gdk(&colour);
	options_save();

}

static void prefswindow_lfontsel(GtkWidget *widget, gpointer data) {

	const char *font;
	
	font = gtk_font_button_get_font_name(GTK_FONT_BUTTON(widget));
	debug_print("PW: Font name '%s'\n", font);
	options_setlocalfont(font);
	options_save();

}

static void prefswindow_ofontsel(GtkWidget *widget, gpointer data) {

	const char *font;
	
	font = gtk_font_button_get_font_name(GTK_FONT_BUTTON(widget));
	debug_print("PW: Font name '%s'\n", font);
	options_setofont(font);
	options_save();

}

static void prefswindow_ocolsel(GtkWidget *widget, gpointer data) {

	GdkColor colour;
	
	gtk_color_button_get_color(GTK_COLOR_BUTTON(widget), &colour);
	options_setocolour_gdk(&colour);
	options_save();

}

static void prefswindow_ofontoverride_toggle(GtkWidget *widget, gpointer data) {

	if ( gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)) ) {
		options_setofontoverride(TRUE);
		gtk_widget_set_sensitive(prefswindow.ofontbutton, TRUE);
		gtk_widget_set_sensitive(prefswindow.ocolourbutton, TRUE);
	} else {
		options_setofontoverride(FALSE);
		gtk_widget_set_sensitive(prefswindow.ofontbutton, FALSE);
		gtk_widget_set_sensitive(prefswindow.ocolourbutton, FALSE);
	}
	options_save();

}

static void prefswindow_ircstyle_toggle(GtkWidget *widget, gpointer data) {

	if ( gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)) ) {
		options_setircstyle(TRUE);
	} else {
		options_setircstyle(FALSE);
	}
	options_save();

}

static void prefswindow_timestamps_toggle(GtkWidget *widget, gpointer data) {

	if ( gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)) ) {
		options_settimestamps(TRUE);
	} else {
		options_settimestamps(FALSE);
	}
	options_save();

}

static void prefswindow_showavatars_toggle(GtkWidget *widget, gpointer data) {

	if ( gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)) ) {
		options_setshowavatars(TRUE);
	} else {
		options_setshowavatars(FALSE);
	}
	options_save();

}

static gint prefswindow_validate_wget(GtkWidget *widget, gpointer data) {

	const char *text = gtk_entry_get_text(GTK_ENTRY(widget));
	options_setwget(text);
	options_save();
	
	return FALSE;

}

static void prefswindow_do_wget() {
	gtk_entry_set_text(GTK_ENTRY(prefswindow.wget), options_wget());
}

static gint prefswindow_guess_wget(GtkWidget *widget, gpointer data) {

	char *nexus;
	
	options_setwget("/usr/bin/wget");
	nexus = twnauth_loginurl();
	if ( strlen(nexus) == 0 ) {
		free(nexus);
		options_setwget("/usr/bin/wget --no-check-certificate");
		nexus = twnauth_loginurl();
		if ( strlen(nexus) == 0 ) {
			free(nexus);
			options_setwget("/usr/local/bin/wget");
			nexus = twnauth_loginurl();
			if ( strlen(nexus) == 0 ) {
				free(nexus);
				options_setwget("/usr/local/bin/wget --no-check-certificate");
				nexus = twnauth_loginurl();
				if ( strlen(nexus) == 0 ) {
					free(nexus);
					options_setwget("~/bin/wget");
					nexus = twnauth_loginurl();
					if ( strlen(nexus) == 0 ) {
						free(nexus);
						options_setwget("~/bin/wget --no-check-certificate");
						nexus = twnauth_loginurl();
						if ( strlen(nexus) == 0 ) {
							free(nexus);
							options_setwget("wget");
							nexus = twnauth_loginurl();
							if ( strlen(nexus) == 0 ) {
								free(nexus);
								options_setwget("wget --no-check-certificate");
								nexus = twnauth_loginurl();
								if ( strlen(nexus) == 0 ) {
									error_report("Couldn't determine correct wget configuration - is wget installed on your system?");
									free(nexus);
									return FALSE;
								}
							}
						}
					}
				}
			}
		}
	}
	
	free(nexus);
	prefswindow_do_wget();
	error_message("Successfully determined wget configuration.");

	return FALSE;

}

static gint prefswindow_check_wget(GtkWidget *widget, gpointer data) {

	char *nexus;
	
	nexus = twnauth_loginurl();
	
	if ( strlen(nexus) != 0 ) {
		GtkWidget *window;
		window = gtk_message_dialog_new(GTK_WINDOW(prefswindow.window), GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, "'wget' configuration appears to be correct.");
		g_signal_connect_swapped(window, "response", G_CALLBACK(gtk_widget_destroy), window);
		gtk_widget_show(window);
	} else {
		GtkWidget *window;
		window = gtk_message_dialog_new(GTK_WINDOW(prefswindow.window), GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_WARNING, GTK_BUTTONS_CLOSE, "'wget' appears not to work...");
		g_signal_connect_swapped(window, "response", G_CALLBACK(gtk_widget_destroy), window);
		gtk_widget_show(window);
	}
	
	free(nexus);
	
	return FALSE;

}

static gint prefswindow_showemoticons_toggle(GtkWidget *widget, gpointer data) {

	if ( gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)) ) {
		options_setshowemoticons(TRUE);
	} else {
		options_setshowemoticons(FALSE);
	}
	options_save();

	return FALSE;

}

/* Open the preferences window */
void prefswindow_open(int page) {
		
	GtkWidget *prefs_notebook;
	
	GtkWidget *prefs_auth_label;
	GtkWidget *prefs_auth_box;	
	GtkWidget *prefs_auth_wget_label;
	GtkWidget *prefs_auth_wget_guess;
	GtkWidget *prefs_auth_wget_check;
	GtkWidget *prefs_auth_wget_hbox;
	GtkWidget *prefs_auth_wget_vbox;
	GtkWidget *prefs_auth_wget_nbox;
	GtkWidget *prefs_auth_wget_heading;
	GtkWidget *prefs_auth_wget_heading_justify;

	/* Arrrggghhhhh */
	GtkWidget *prefs_messagewindows_label;
	GtkWidget *prefs_messagewindows_box;
	GtkWidget *prefs_messagewindows_text;
	GtkWidget *prefs_messagewindows_font_box;
	GtkWidget *prefs_messagewindows_font_button;
	GtkWidget *prefs_messagewindows_font_label;
	GtkWidget *prefs_messagewindows_font_label_justify;
	GtkWidget *prefs_messagewindows_colour_button;
	GtkWidget *prefs_messagewindows_ofont_box;
	GtkWidget *prefs_messagewindows_ofont_label;
	GtkWidget *prefs_messagewindows_ofont_label_justify;
	GtkWidget *prefs_messagewindows_ofont_override;
	GtkWidget *prefs_messagewindows_font_hbox;
	GtkWidget *prefs_messagewindows_font_vbox;
	GtkWidget *prefs_messagewindows_ofont_hbox;
	GtkWidget *prefs_messagewindows_ofont_vbox;
	GtkWidget *prefs_messagewindows_fbox;
	GtkWidget *prefs_messagewindows_nbox;
	GtkWidget *prefs_messagewindows_qbox;
	GtkWidget *prefs_messagewindows_toggles_vbox;
	GtkWidget *prefs_messagewindows_toggles_hbox;
	GtkWidget *prefs_messagewindows_toggles_label;
	GtkWidget *prefs_messagewindows_toggles_label_justify;
	GtkWidget *prefs_messagewindows_toggles_irc;
	GtkWidget *prefs_messagewindows_toggles_timestamp;
	GtkWidget *prefs_messagewindows_toggles_avatars;
	GtkWidget *prefs_messagewindows_toggles_emoticons;

	/* Prevent opening of the preferences window more than once */
	if ( prefswindow.open == 1 ) {
		return;
	}

	prefswindow.window = gtk_dialog_new_with_buttons("TuxMessenger Preferences", mainwindow_gtkwindow(), 0, GTK_STOCK_CLOSE, GTK_RESPONSE_ACCEPT, NULL);
	gtk_dialog_set_has_separator(GTK_DIALOG(prefswindow.window), FALSE);
	
	prefs_notebook = gtk_notebook_new();
	gtk_notebook_set_tab_pos(GTK_NOTEBOOK(prefs_notebook), GTK_POS_TOP);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(prefswindow.window)->vbox), prefs_notebook, TRUE, TRUE, 0);
		
	/* ****************** Message Windows *************************************************************/

	prefs_messagewindows_label = gtk_label_new("Message Windows");
	prefs_messagewindows_box = gtk_vbox_new(FALSE, 0);
	gtk_container_set_border_width(GTK_CONTAINER(prefs_messagewindows_box), 12);
	gtk_notebook_append_page(GTK_NOTEBOOK(prefs_notebook), prefs_messagewindows_box, prefs_messagewindows_label);

	prefs_messagewindows_text = gtk_label_new("");
	gtk_label_set_markup(GTK_LABEL(prefs_messagewindows_text), "<span style=\"italic\" weight=\"light\">These settings define the behaviour of newly-created instant message windows.  You can also set all of these options individually using the menus for each window.</span>");
	gtk_widget_set_size_request(GTK_WIDGET(prefs_messagewindows_text), 500, -1);
	gtk_label_set_line_wrap(GTK_LABEL(prefs_messagewindows_text), TRUE);
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_box), prefs_messagewindows_text, FALSE, FALSE, 0);

	prefs_messagewindows_fbox = gtk_vbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_box), prefs_messagewindows_fbox, FALSE, FALSE, 12);
	prefs_messagewindows_font_label = gtk_label_new("");
	prefs_messagewindows_font_label_justify = gtk_hbox_new(FALSE, 0);
	gtk_label_set_markup(GTK_LABEL(prefs_messagewindows_font_label), "<span weight=\"bold\">Font and Colour for Your Messages</span>");
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_font_label_justify), prefs_messagewindows_font_label, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_fbox), prefs_messagewindows_font_label_justify, FALSE, FALSE, 6);

	prefs_messagewindows_font_vbox = gtk_vbox_new(FALSE, 0);
	prefs_messagewindows_font_hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_fbox), prefs_messagewindows_font_hbox, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_font_hbox), prefs_messagewindows_font_vbox, FALSE, FALSE, 12);
	
	prefs_messagewindows_font_box = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_font_vbox), prefs_messagewindows_font_box, FALSE, FALSE, 0);
	if ( options_localfont() ) {
		prefs_messagewindows_font_button = gtk_font_button_new_with_font(options_localfont());
	} else {
		prefs_messagewindows_font_button = gtk_font_button_new();
	}
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_font_box), prefs_messagewindows_font_button, FALSE, FALSE, 6);
	g_signal_connect(G_OBJECT(prefs_messagewindows_font_button), "font-set", GTK_SIGNAL_FUNC(prefswindow_lfontsel), NULL);

	if ( options_localcolour_gdk() == NULL ) {
		prefs_messagewindows_colour_button = gtk_color_button_new();
	} else {
		prefs_messagewindows_colour_button = gtk_color_button_new_with_color(options_localcolour_gdk());
	}
	g_signal_connect(G_OBJECT(prefs_messagewindows_colour_button), "color-set", GTK_SIGNAL_FUNC(prefswindow_lcolsel), NULL);
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_font_box), prefs_messagewindows_colour_button, FALSE, FALSE, 6);

	prefs_messagewindows_nbox = gtk_vbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_box), prefs_messagewindows_nbox, FALSE, FALSE, 12);
	prefs_messagewindows_ofont_label = gtk_label_new("");
	prefs_messagewindows_ofont_label_justify = gtk_hbox_new(FALSE, 0);
	gtk_label_set_markup(GTK_LABEL(prefs_messagewindows_ofont_label), "<span weight=\"bold\">Fonts and Colours for Contacts' Messages</span>");
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_ofont_label_justify), prefs_messagewindows_ofont_label, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_nbox), prefs_messagewindows_ofont_label_justify, FALSE, FALSE, 0);

	prefs_messagewindows_ofont_vbox = gtk_vbox_new(FALSE, 0);
	prefs_messagewindows_ofont_hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_nbox), prefs_messagewindows_ofont_hbox, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_ofont_hbox), prefs_messagewindows_ofont_vbox, FALSE, FALSE, 12);

	prefs_messagewindows_ofont_override = gtk_check_button_new_with_label("Override contacts' chosen fonts");
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(prefs_messagewindows_ofont_override), options_ofontoverride());

	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_ofont_vbox), prefs_messagewindows_ofont_override, FALSE, FALSE, 5);
	prefs_messagewindows_ofont_box = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_ofont_vbox), prefs_messagewindows_ofont_box, FALSE, FALSE, 0);
	if ( options_ofont() ) {
		prefswindow.ofontbutton = gtk_font_button_new_with_font(options_ofont());
	} else {
		prefswindow.ofontbutton = gtk_font_button_new();
	}
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_ofont_box), prefswindow.ofontbutton, FALSE, FALSE, 6);
	g_signal_connect(G_OBJECT(prefswindow.ofontbutton), "font-set", GTK_SIGNAL_FUNC(prefswindow_ofontsel), NULL);
	if ( options_ocolour_gdk() == NULL ) {
		prefswindow.ocolourbutton = gtk_color_button_new();
	} else {
		prefswindow.ocolourbutton = gtk_color_button_new_with_color(options_ocolour_gdk());
	}
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_ofont_box), prefswindow.ocolourbutton, FALSE, FALSE, 6);
	g_signal_connect(G_OBJECT(prefswindow.ocolourbutton), "color-set", GTK_SIGNAL_FUNC(prefswindow_ocolsel), NULL);

	/* Call the check button's callback to sort out greying-out of stuff as appropriate.
		Results in a spurious option setting, but saves code duplication. */
	prefswindow_ofontoverride_toggle(prefs_messagewindows_ofont_override, NULL);
	/* NOW connect the signal handler.  Doing it any earlier makes Bad Stuff happen. */
	g_signal_connect(G_OBJECT(prefs_messagewindows_ofont_override), "toggled", GTK_SIGNAL_FUNC(prefswindow_ofontoverride_toggle), prefs_messagewindows_ofont_box);

	prefs_messagewindows_qbox = gtk_vbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_box), prefs_messagewindows_qbox, FALSE, FALSE, 12);
	prefs_messagewindows_toggles_label = gtk_label_new("");
	prefs_messagewindows_toggles_label_justify = gtk_hbox_new(FALSE, 0);
	gtk_label_set_markup(GTK_LABEL(prefs_messagewindows_toggles_label), "<span weight=\"bold\">Text and Window Layout</span>");
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_toggles_label_justify), prefs_messagewindows_toggles_label, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_qbox), prefs_messagewindows_toggles_label_justify, FALSE, FALSE, 0);

	prefs_messagewindows_toggles_vbox = gtk_vbox_new(FALSE, 0);
	prefs_messagewindows_toggles_hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_qbox), prefs_messagewindows_toggles_hbox, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_toggles_hbox), prefs_messagewindows_toggles_vbox, FALSE, FALSE, 12);

	prefs_messagewindows_toggles_irc = gtk_check_button_new_with_label("Use IRC style for messages");
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_toggles_vbox), prefs_messagewindows_toggles_irc, FALSE, FALSE, 3);
	prefs_messagewindows_toggles_timestamp = gtk_check_button_new_with_label("Timestamp messages");
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_toggles_vbox), prefs_messagewindows_toggles_timestamp, FALSE, FALSE, 3);
	prefs_messagewindows_toggles_avatars = gtk_check_button_new_with_label("Display avatars");
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_toggles_vbox), prefs_messagewindows_toggles_avatars, FALSE, FALSE, 3);
	prefs_messagewindows_toggles_emoticons = gtk_check_button_new_with_label("Display emoticons");
	gtk_box_pack_start(GTK_BOX(prefs_messagewindows_toggles_vbox), prefs_messagewindows_toggles_emoticons, FALSE, FALSE, 3);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(prefs_messagewindows_toggles_irc), options_ircstyle());
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(prefs_messagewindows_toggles_timestamp), options_timestamps());
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(prefs_messagewindows_toggles_avatars), options_showavatars());
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(prefs_messagewindows_toggles_emoticons), options_showemoticons());
	g_signal_connect(G_OBJECT(prefs_messagewindows_toggles_irc), "toggled", GTK_SIGNAL_FUNC(prefswindow_ircstyle_toggle), NULL);
	g_signal_connect(G_OBJECT(prefs_messagewindows_toggles_timestamp), "toggled", GTK_SIGNAL_FUNC(prefswindow_timestamps_toggle), NULL);
	g_signal_connect(G_OBJECT(prefs_messagewindows_toggles_avatars), "toggled", GTK_SIGNAL_FUNC(prefswindow_showavatars_toggle), NULL);
	g_signal_connect(G_OBJECT(prefs_messagewindows_toggles_emoticons), "toggled", GTK_SIGNAL_FUNC(prefswindow_showemoticons_toggle), NULL);
	
	/* ****************** Authentication *************************************************************/

	prefs_auth_label = gtk_label_new("Authentication");
	prefs_auth_box = gtk_vbox_new(FALSE, 0);
	gtk_container_set_border_width(GTK_CONTAINER(prefs_auth_box), 12);
	gtk_notebook_append_page(GTK_NOTEBOOK(prefs_notebook), prefs_auth_box, prefs_auth_label);

	prefs_auth_wget_heading = gtk_label_new("");
	gtk_label_set_markup(GTK_LABEL(prefs_auth_wget_heading), "<span weight=\"bold\">Command for 'wget'</span>");
	prefs_auth_wget_heading_justify = gtk_hbox_new(FALSE, 0);
	prefs_auth_wget_nbox = gtk_vbox_new(FALSE, 2);
	gtk_box_pack_start(GTK_BOX(prefs_auth_wget_nbox), prefs_auth_wget_heading_justify, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(prefs_auth_wget_heading_justify), prefs_auth_wget_heading, FALSE, FALSE, 0);

	gtk_box_pack_start(GTK_BOX(prefs_auth_box), prefs_auth_wget_nbox, FALSE, FALSE, 12);
	prefs_auth_wget_hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(prefs_auth_wget_nbox), prefs_auth_wget_hbox, FALSE, FALSE, 0);
	prefs_auth_wget_vbox = gtk_vbox_new(FALSE, 6);
	gtk_box_pack_start(GTK_BOX(prefs_auth_wget_hbox), prefs_auth_wget_vbox, FALSE, FALSE, 12);
	
	prefs_auth_wget_label = gtk_label_new("");
	gtk_label_set_markup(GTK_LABEL(prefs_auth_wget_label), "<span style=\"italic\" weight=\"light\">TuxMessenger calls the 'wget' program to help log in to the server.  You need to specify the command to run to invoke a copy of 'wget' which is capable of using HTTPS.  In most cases you can simply enter 'wget' here.</span>");
	gtk_widget_set_size_request(GTK_WIDGET(prefs_auth_wget_label), 500, -1);
	gtk_label_set_line_wrap(GTK_LABEL(prefs_auth_wget_label), TRUE);
	gtk_box_pack_start(GTK_BOX(prefs_auth_wget_vbox), prefs_auth_wget_label, FALSE, FALSE, 0);

	prefswindow.wget = gtk_entry_new_with_max_length(128);
	prefswindow_do_wget();
	gtk_box_pack_start(GTK_BOX(prefs_auth_wget_vbox), prefswindow.wget, FALSE, FALSE, 0);
	g_signal_connect(G_OBJECT(prefswindow.wget), "focus-out-event", GTK_SIGNAL_FUNC(prefswindow_validate_wget), NULL);
	g_signal_connect(G_OBJECT(prefswindow.wget), "activate", GTK_SIGNAL_FUNC(prefswindow_validate_wget), NULL);

	prefs_auth_wget_guess = gtk_button_new_with_label("Attempt to automatically determine correct wget configuration");
	gtk_box_pack_start(GTK_BOX(prefs_auth_wget_vbox), prefs_auth_wget_guess, FALSE, FALSE, 0);
	g_signal_connect(G_OBJECT(prefs_auth_wget_guess), "clicked", GTK_SIGNAL_FUNC(prefswindow_guess_wget), NULL);

	prefs_auth_wget_check = gtk_button_new_with_label("Check wget configuration");
	gtk_box_pack_start(GTK_BOX(prefs_auth_wget_vbox), prefs_auth_wget_check, FALSE, FALSE, 0);
	g_signal_connect(G_OBJECT(prefs_auth_wget_check), "clicked", GTK_SIGNAL_FUNC(prefswindow_check_wget), NULL);
	
	/* ************************************************************************************************/

	gtk_window_set_title(GTK_WINDOW(prefswindow.window), "TuxMessenger Preferences");
	gtk_window_position(GTK_WINDOW(prefswindow.window), GTK_WIN_POS_MOUSE);
	gtk_window_set_policy(GTK_WINDOW(prefswindow.window), FALSE, TRUE, FALSE);
	
	gtk_widget_show_all(prefswindow.window);
	gtk_notebook_set_page(GTK_NOTEBOOK(prefs_notebook), page);

	g_signal_connect(G_OBJECT(prefswindow.window), "destroy", GTK_SIGNAL_FUNC(prefswindow_closed), NULL);
	g_signal_connect(G_OBJECT(prefswindow.window), "response", GTK_SIGNAL_FUNC(gtk_widget_destroy), NULL);

	prefswindow.open = 1;
		
}
