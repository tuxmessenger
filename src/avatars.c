/*
 * avatars.h
 *
 * Handling of User Display Pictures (but not displaying them)
 *	N.B. msnp2p.c does most of the work of actually sending
 *	and receiving them on the network.
 *
 * (c) 2002-20045Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <assert.h>
#include <openssl/sha.h>

#include "sbsessions.h"
#include "debug.h"
#include "routines.h"
#include "xml.h"
#include "options.h"

char *avatars_getlocalfilename(const char *name) {

	char *filename;
	char *done;
	
	filename = malloc(strlen(name)+17);
	strcpy(filename, "~/.tuxmessenger/");
	strcat(filename, name);
	
	done = routines_glob(filename);
	free(filename);
	
	return done;

}


static char *avatars_getfilename(const char *sha1d) {

	char *filename;
	char *filename2;
	char *done;

	filename = xml_killillegalchars(sha1d);
	
	filename2 = malloc(strlen(filename)+25);
	strcpy(filename2, "~/.tuxmessenger/avatars/");
	strcat(filename2, filename);
	free(filename);
	
	done = routines_glob(filename2);
	free(filename2);
	
	return done;

}

/* Check if the picture for a user is already downloaded.  Returns the filename if it's here. */
char *avatars_havepicture(const char *dpobject) {

	char *picture_data_hash;
	struct stat stat_buffer;
	struct stat *statbuf;
	char *dpobject_decoded;
	char *full_filename;

	assert(dpobject != NULL);
	
	dpobject_decoded = routines_urldecode(dpobject);
	picture_data_hash = xml_getfield(dpobject_decoded, "SHA1D");
	free(dpobject_decoded);
	full_filename = avatars_getfilename(picture_data_hash);
	free(picture_data_hash);
	if ( full_filename == NULL ) {
		return NULL;
	}
		
	/* Check if we already have this picture or not */
	statbuf = &stat_buffer;
	debug_print("AV: Looking for '%s': ", full_filename);
	if ( stat(full_filename, statbuf) != -1 ) {
		debug_print("found!\n");
		return full_filename;
	} else {
		debug_print("not found.\n");
	}
	free(full_filename);

	return NULL;

}

/* Return filename of the image to use while downloading a picture. */
char *avatars_default_fetching() {

	return avatars_getlocalfilename("wait_avatar.png");

}

/* Return filename of default image to use when a user has no picture. */
char *avatars_default_none() {

	return avatars_getlocalfilename("default_avatar.png");

}

/* Return filename of image to use for local user. */
char *avatars_local() {

	return avatars_getlocalfilename("avatar.png");

}

char *avatars_localobject() {

	char *msnobject;
	char *msnobject_coded;
	char *sha1c_b64;
	char *sha1d_b64;
	char *sha1c;
	char *sha1d;
	struct stat stat_buffer;
	struct stat *statbuf;
	void *picture_data;
	size_t size;
	FILE *fh;
	char *filename;
	
	filename = avatars_local();
	if ( filename == NULL ) {
		debug_print("AV: No avatar found.\n");
		return strdup("0");
	}

	statbuf = &stat_buffer;
	if ( stat(filename, statbuf) == -1 ) {
		debug_print("AV: No avatar found.\n");
		return strdup("0");
	}
	
	size = (int)statbuf->st_size;
	
	fh = fopen(filename, "r");
	picture_data = malloc(size);
	if ( fread(picture_data, size, 1, fh) < 0 ) {
		debug_print("AV: Couldn't open avatar file.\n");
		return strdup("0");
	}
	fclose(fh);	
	sha1d = SHA1(picture_data, size, NULL);
	sha1d_b64 = routines_base64givenlength(sha1d, 20);
	free(picture_data);

	msnobject = malloc(256);
	sprintf(msnobject, "Creator%sSize%iType3LocationTFR6B.tmpFriendlyAAA=SHA1D%s", options_username(), size, sha1d_b64);
	sha1c = SHA1(msnobject, strlen(msnobject), NULL);
	sha1c_b64 = routines_base64givenlength(sha1c, 20);

	debug_print("AV: Avatar: %s - %i bytes\n", filename, size);
	debug_print("AV: SHA1D: %s\n", sha1d_b64);
	
	sprintf(msnobject, "<msnobj Creator=\"%s\" Size=\"%i\" Type=\"3\" Location=\"TFR6B.tmp\" Friendly=\"AAA=\" SHA1D=\"%s\" SHA1C=\"%s\"/>", options_username(), size, sha1d_b64, sha1c_b64);
	free(sha1d_b64);
	free(sha1c_b64);
	
	msnobject_coded = routines_urlencode(msnobject);
	free(msnobject);
	free(filename);
	
	return msnobject_coded;

}

char *avatars_unwrapsha1d(const char *dpobject) {

	char *dpobject_decoded;
	char *data_hash;

	dpobject_decoded = routines_urldecode(dpobject);
	data_hash = xml_getfield(dpobject_decoded, "SHA1D");
	free(dpobject_decoded);
	
	return data_hash;

}
