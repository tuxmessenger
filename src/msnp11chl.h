/*
 * msnp11.h
 *
 * New-style challenge
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 *
 *		Special Notice for the MSNP11 Challenge Module
 *		----------------------------------------------
 *		The author(s) of this software performed no
 *		reverse-engineering of any Microsoft software
 *		in order to create this module.  Information
 *		on the MSNP11-style challenge can be freely
 *		found on the Web.
 *
 */

#ifndef MSNP11CHL_H
#define MSNP11CHL_H

extern char *msnp11chl_response(const char *challenge);

//#define CLIENT_ID "PROD0104U6VVM{UJ"
//#define CLIENT_CODE "VK67B}379XYM5}$T"

#define CLIENT_ID "PROD0101{0RM?UBW"
#define CLIENT_CODE "CFHUR$52U_{VIX5T"

//#define CLIENT_ID "PROD0090YUAUV{2B"
//#define CLIENT_CODE "YMM8C_H7KCQ2S_KL"

#endif	/* MSNP11CHL_H */
