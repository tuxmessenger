/*
 * accountwindow.c
 *
 * The "set account details" window
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>
#include <stdlib.h>
#include <string.h>

#include "debug.h"
#include "options.h"
#include "mainwindow.h"
#include "routines.h"
#include "error.h"

static int accountwindow_isopen = 0;

static int accountwindow_validate_username(GtkWidget *widget) {

	options_setusername(gtk_entry_get_text(GTK_ENTRY(widget)));
	mainwindow_kickdispatch();
	return FALSE;

}

static int accountwindow_validate_password(GtkWidget *widget) {

	options_setpassword(gtk_entry_get_text(GTK_ENTRY(widget)));
	mainwindow_kickdispatch();
	return FALSE;

}

static int accountwindow_validate_server(GtkWidget *widget) {

	char *newserver;
	const char *server = gtk_entry_get_text(GTK_ENTRY(widget));
	char *hostname = routines_hostname(server);
		
	options_sethostname(hostname);
	free(hostname);
	options_setport(routines_port(server));
	
	/* options_sethostname and options_setport will change the values if they're obviously wrong. */
	newserver = malloc(strlen(options_hostname())+6);
	/* options_port() is "unsigned short int" so max 16384 = 5 chars */
	sprintf(newserver, "%s:%i", options_hostname(), options_port());
	gtk_entry_set_text(GTK_ENTRY(widget), newserver);
	free(newserver);
	
	mainwindow_kickdispatch();
	
	return FALSE;

}

static int accountwindow_validate_remember(GtkWidget *widget) {

	int remember;

	g_object_get(G_OBJECT(widget), "active", &remember, NULL);
	debug_print("AW: remember: %i\n", remember);
	options_setrememberlogindetails(remember);
	
	return FALSE;

}

static int accountwindow_closed() {

	accountwindow_isopen = 0;
	options_save();

	if ( strlen(options_username()) == 0 ) {
		error_report("You must enter a username!");
	}

	return FALSE;

}

void accountwindow_open() {

	char *hostnameport;
	GtkWidget *window;
	GtkWidget *border_hbox;
	GtkWidget *border_vbox;
	
	GtkWidget *username;
	GtkWidget *username_justify;
	GtkWidget *username_hbox;
	GtkWidget *username_spacer;
	
	GtkWidget *password;
	GtkWidget *password_justify;
	GtkWidget *password_hbox;
	GtkWidget *password_spacer;
	
	GtkWidget *server;
	GtkWidget *server_justify;
	GtkWidget *server_hbox;
	GtkWidget *server_spacer;
	
	GtkWidget *remember;
	
	if ( accountwindow_isopen != 0 ) {
		debug_print("AW: Account details window already open.\n");
		return;
	}

	window = gtk_dialog_new_with_buttons("Configure Account Details", mainwindow_gtkwindow(), 0, GTK_STOCK_CLOSE, GTK_RESPONSE_ACCEPT, NULL);
	border_hbox = gtk_hbox_new(FALSE, 0);
	border_vbox = gtk_vbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(border_hbox), border_vbox, FALSE, FALSE, 6);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(window)->vbox), border_hbox, FALSE, FALSE, 6);
	
	/* "Username" entry box */
	username_hbox = gtk_hbox_new(FALSE, 0);
	username_justify = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(username_justify), gtk_label_new("Username:"), FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(username_hbox), username_justify, FALSE, FALSE, 0);
	username = gtk_entry_new_with_max_length(255);
	gtk_box_pack_end(GTK_BOX(username_hbox), username, FALSE, FALSE, 0);
	username_spacer = gtk_label_new("");
	gtk_widget_set_size_request(username_spacer, 12, -1);
	gtk_box_pack_end(GTK_BOX(username_hbox), username_spacer, FALSE, FALSE, 0);
	g_object_set(G_OBJECT(username), "width-chars", 32, NULL);
	g_signal_connect(G_OBJECT(username), "focus-out-event", GTK_SIGNAL_FUNC(accountwindow_validate_username), NULL);
	g_signal_connect(G_OBJECT(username), "activate", GTK_SIGNAL_FUNC(accountwindow_validate_username), NULL);
	gtk_box_pack_start(GTK_BOX(border_vbox), username_hbox, FALSE, FALSE, 6);

	/* "Password" entry box */
	password_hbox = gtk_hbox_new(FALSE, 0);
	password_justify = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(password_justify), gtk_label_new("Password:"), FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(password_hbox), password_justify, FALSE, FALSE, 0);
	password = gtk_entry_new_with_max_length(255);
	gtk_box_pack_end(GTK_BOX(password_hbox), password, FALSE, FALSE, 0);
	password_spacer = gtk_label_new("");
	gtk_widget_set_size_request(password_spacer, 12, -1);
	gtk_box_pack_end(GTK_BOX(password_hbox), password_spacer, FALSE, FALSE, 0);
	g_object_set(G_OBJECT(password), "width-chars", 32, NULL);
	gtk_entry_set_visibility(GTK_ENTRY(password), FALSE);
	g_signal_connect(G_OBJECT(password), "focus-out-event", GTK_SIGNAL_FUNC(accountwindow_validate_password), NULL);
	g_signal_connect(G_OBJECT(password), "activate", GTK_SIGNAL_FUNC(accountwindow_validate_password), NULL);
	gtk_box_pack_start(GTK_BOX(border_vbox), password_hbox, FALSE, FALSE, 6);

	/* "Server" entry box */
	server_hbox = gtk_hbox_new(FALSE, 0);
	server_justify = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(server_justify), gtk_label_new("Server:"), FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(server_hbox), server_justify, FALSE, FALSE, 0);
	server = gtk_entry_new_with_max_length(255);
	gtk_box_pack_end(GTK_BOX(server_hbox), server, FALSE, FALSE, 0);
	server_spacer = gtk_label_new("");
	gtk_widget_set_size_request(server_spacer, 12, -1);
	gtk_box_pack_end(GTK_BOX(server_hbox), server_spacer, FALSE, FALSE, 0);
	g_object_set(G_OBJECT(server), "width-chars", 32, NULL);
	g_signal_connect(G_OBJECT(server), "focus-out-event", GTK_SIGNAL_FUNC(accountwindow_validate_server), NULL);
	g_signal_connect(G_OBJECT(server), "activate", GTK_SIGNAL_FUNC(accountwindow_validate_server), NULL);
	gtk_box_pack_start(GTK_BOX(border_vbox), server_hbox, FALSE, FALSE, 6);
		
	/* Rememer login details check-box */
	remember = gtk_check_button_new_with_label("Remember login details");
	gtk_box_pack_start(GTK_BOX(border_vbox), remember, FALSE, FALSE, 6);
	g_signal_connect(G_OBJECT(remember), "toggled", GTK_SIGNAL_FUNC(accountwindow_validate_remember), NULL);
		
	/* Fill the fields in... */
	gtk_entry_set_text(GTK_ENTRY(username), options_username());
	gtk_entry_set_text(GTK_ENTRY(password), options_password());
	hostnameport = malloc(strlen(options_hostname())+6);
	/* options_port() < 65536 because of data size - so always fits. */
	sprintf(hostnameport, "%s:%i", options_hostname(), options_port());
	gtk_entry_set_text(GTK_ENTRY(server), hostnameport);
	free(hostnameport);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(remember), options_rememberlogindetails());

	gtk_widget_show_all(window);
	g_signal_connect(G_OBJECT(window), "destroy", GTK_SIGNAL_FUNC(accountwindow_closed), NULL);
	g_signal_connect(G_OBJECT(window), "response", GTK_SIGNAL_FUNC(gtk_widget_destroy), NULL);
	accountwindow_isopen = 1;

}
