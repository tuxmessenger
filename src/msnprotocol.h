/*
 * msnprotocol.h
 *
 * Low-level DS/NS protocol handling
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifndef MSNPROTOCOL_H
#define MSNPROTOCOL_H

#include <gtk/gtk.h>
#include "msngenerics.h"
#include "sbsessions.h"

extern void msnprotocol_setstatus(OnlineState newstatus);
extern void msnprotocol_connect(const char *hostname, unsigned short int port);
extern int msnprotocol_signedin(void);
extern int msnprotocol_disconnected(void);
extern int msnprotocol_initiatesb(void);
extern void msnprotocol_requestsignout(void);
extern void msnprotocol_setmfn(const char *new_mfn);
extern void msnprotocol_setcsm(const char *new_csm);
extern void msnprotocol_adduser(const char *username, const char *list);
extern void msnprotocol_adduserfriendly(const char *username, const char *friendlyname, const char *list);
extern void msnprotocol_remuser(const char *username, const char *list);
extern OnlineState msnprotocol_status(void);

#endif	/* MSNPROTOCOL_H */
