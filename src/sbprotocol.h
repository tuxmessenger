/*
 * sbprotocol.h
 * 
 * SB protocol handling
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifndef SBPROTOCOL_H
#define SBPROTOCOL_H

#include "sbsessions.h"

/* Session initiation hooks. */
extern void sbprotocol_initiate_local(unsigned int trid, const char *hostname, unsigned int port, const char *cki_key);
extern void sbprotocol_initiate_remote(SbSession *session, const char *switchboardaddress, const char *sessionid, const char *authchallenge);

/* High-level protocol operations. */
extern void sbprotocol_invite(SbSession *session, const char *username);
extern void sbprotocol_leavesession(SbSession *session);
extern void sbprotocol_sendtypingcontrol(SbSession *session);
extern void sbprotocol_sendnudge(SbSession *session);

/* Low-level protocol operations. */
extern void sbprotocol_send(SbSession *session, char *textblock, int length);
extern int sbprotocol_sendtr_nonewline(SbSession *session, const char *instr, const char *args, ssize_t length);
extern void sbprotocol_parsemsg(SbSession *session, const char *msg, ssize_t msglength);
extern void sbprotocol_close(SbSession *session);

#endif	/* SBPROTOCOL_H */
