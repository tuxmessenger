/*
 * mainwindow.h
 * 
 * The main (contact list) window
 *
 * (c) 2002-2007 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "msngenerics.h"

#include <gtk/gtk.h>
/* This is meant to be an opaque data type.  Don't try anything clever... */
typedef struct {

	GtkWidget *hbox;		/* Overall hbox */
	GtkWidget *adjustbutton;	/* Button with popup menu and icon */
	GtkWidget *button;		/* The main button itself (only if online) */
	GtkWidget *label;		/* Label containing the friendly name. */
	GtkWidget *eventbox;		/* Eventbox for tooltips (only if offline) */
	GtkWidget *pixmap;		/* Pixmap widget to go inside adjustbutton */
	GtkTooltips *tooltips;		/* Tooltip containing the username and other details */

} UIContact;

extern void mainwindow_open(void);
extern void mainwindow_setdispatch(void);
extern void mainwindow_setonline(void);
extern void mainwindow_setmfn(const char *new_friendlyname);
extern UIContact *mainwindow_addcontact(char *username, char *friendlyname, OnlineState status);
extern void mainwindow_setcontactstatus(UIContact *uicontact, char *username, char *friendlyname, OnlineState status);
extern void mainwindow_addaccelgroup(GtkAccelGroup *accel_group);
extern void mainwindow_destroycontact(UIContact *uicontact);
extern void mainwindow_removecontact(UIContact *uicontact);
extern GtkStyle *mainwindow_style(void);
extern GdkWindow *mainwindow_window(void);
extern GtkWindow *mainwindow_gtkwindow(void);
extern void mainwindow_fname_startchange(void);
extern void mainwindow_kickdispatch(void);
extern void mainwindow_forcestatus(OnlineState status);
extern void mainwindow_setcsm(const char *new_csm);

#endif	/* MAINWINDOW_H */
