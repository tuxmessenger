/*
 * sbsessions.h
 * 
 * SB session management (but not the protocol nor UI)
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifndef SBSESSIONS_H
#define SBSESSIONS_H

#include <gtk/gtk.h>

typedef enum {
	SESSION_SOURCE_LOCAL,	/* Session was initiated by the user clicking on a contact. */
	SESSION_SOURCE_REMOTE	/* Session was initiated by an RNG message from Outside. */
} SbSessionSource;

typedef enum {
	SBCONMODE_LINE,		/* Normal, line-based, communication. */
	SBCONMODE_MSG		/* A message is coming through. */
} SbConMode;

typedef struct _sbuser {		/* No data is here that's already held by src/contactlist.c, except the username. */

	struct _sbuser *next;
	
	char *username;
	
	char *dpsha1d;			/* SHA1D field from their picture as it appears in the IM window. */
		
} SbUser;

/* Data structure to hold information while reconstructing a multipacketed message. */
typedef struct _multipacketmsg {

	struct _multipacketmsg *next;
	struct _multipacketmsg *previous;

	char *message_id;
	int chunks_expected;
	int chunks_received;
	char *data;
	size_t datalength;

} MultiPacketMsg;

typedef struct _cachedmsg {

	struct _cachedmsg *next;
	char *message;
	size_t length;

} CachedMsg;

typedef struct _sbsession {

	struct _sbsession *next;
	
	/* List of Users */
	SbUser *users;			/* List of people in the session. */
	unsigned int num_users;		/* Number of remote users in this session. Session destroyed if this = zero. */

	/* User interface stuff */
	struct _messagewindow *messagewindow;

	/* High-level protocol stuff */
	int ready;			/* Non-zero if session is up and running. */
	gint ready_timeout;		/* Callback tag to timeout session if it doesn't connect. */
	unsigned int neg_trid;		/* TrID used in negotiating this session. */
	unsigned int trid;		/* TrID used in communicating on the SB (don't confuse with neg_trid) */
	int socket;			/* Socket to the switchboard. */
	SbSessionSource source;		/* Where the session was started from. */
	char *cki_key;			/* Authentication data for negotiating the session. */
	char *sessionid;		/* SessionID (part of authentication to the SB) */
	int first_event;		/* Non-zero if message window currently contains no text */
	char *msg_source;		/* Who the MSG currently being dealt with came from */
	int am_typing;			/* Non-zero if the local user is currently "thought to be typing". */
	int am_typing_callback;		/* Callback tag for the end of each typing period for the local user. */
	char *threeway_intent;		/* Extra user to invite when ready (if any). */
	MultiPacketMsg *multipackets;	/* Linked list of multipacketing records. */
	CachedMsg *cached;		/* Cached messages to send when session is ready. */
	
	/* Low-level protocol stuff */
	int wcallback;			/* Writeable callback */
	char *wbuffer;			/* Write buffer */
	unsigned int wbufsize;		/* Write buffer size */
	unsigned int woffset;		/* Write buffer position */
	int rcallback;			/* Readable callback */
	char *rbuffer;			/* Read buffer */
	unsigned int rbufsize;		/* Read buffer size */
	unsigned int roffset;		/* Read buffer position */
	SbConMode conmode;		/* What's coming through the socket at the moment */
	unsigned int expect_length;	/* Expected length of (eg) a MSG */
	
} SbSession;

#include "messagewindow.h"

/* Operations on SB sessions. */
extern SbSession *sbsessions_create_local(const char *username);
extern SbSession *sbsessions_create_remote(char *username, char *switchboardaddress, char *sessionid, char *authchallenge);
extern SbSession *sbsessions_create_threeway(char *username1, char *username2);
extern void sbsessions_destroy(SbSession *session);
extern void sbsessions_plug(SbSession *session, struct _messagewindow *messagewindow);
extern void sbsessions_unplug(struct _messagewindow *messagewindow);
extern SbSession *sbsessions_find_trid(unsigned int trid);
extern SbSession *sbsessions_find_headless(const char *username);
extern SbSession *sbsessions_find_single_safe(const char *username);
extern int sbsessions_sessionready(SbSession *session);

/* Operations on the local user in SB sessions. */
extern void sbsessions_am_typing(SbSession *session);

/* Operations on other users in SB sessions. */
extern void sbsessions_joined(SbSession *session, char *username, char *friendlyname);
extern void sbsessions_left(SbSession *session, char *username);
extern SbUser *sbsessions_find_username(SbSession *session, const char *username);
extern int sbsessions_stoptyping(SbUser *user);

/* Other stuff. */
extern void sbsessions_destroy_all();

#endif	/* SBSESSIONS_H */
