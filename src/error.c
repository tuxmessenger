/*
 * error.c
 *
 * Report error messages
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>
#include <string.h>

#include "mainwindow.h"

void error_report(const char *message) {
 
	GtkWidget *window;
	
	window = gtk_message_dialog_new(mainwindow_gtkwindow(), GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_WARNING, GTK_BUTTONS_CLOSE, message);
				
	g_signal_connect_swapped(window, "response", G_CALLBACK(gtk_widget_destroy), window);
	gtk_widget_show(window);

}
 
static const char *decode_error(int error_number) {
 
	/* decode server error numbers into understandable messages
		for the User */
	
	switch (error_number) {
		/* I don't pretend to know what all of these mean... */
		
		case 200: return "Server reported Syntax Error";
		case 201: return "Server reported Invalid Parameter";
		case 205: return "Server reported Invalid User";
		case 206: return "Server reported FQDN Missing";
		case 207: return "Already logged in";
		case 208: return "Server reported Invalid Username";
		case 209: return "Invalid friendly name";
		case 210: return "You have too many people on your contact list";
		case 215: return "IGNORE";	/* Already there */
		case 216: return "That user is not on your list";
		case 218: return "Already in that mode";
		case 219: return "Already in opposite list";
		case 280: return "Switchboard request failed";
		case 281: return "NS XFR failed";
		case 300: return "Server reported Required Fields Missing";
		case 302: return "Not logged in";
		case 403: return "List unavailable";
		case 500: return "Internal server error";
		case 501: return "DB server error";
		case 510: return "File operation error";
		case 520: return "Memory allocation error";
		case 540: return "Failed challenge";
		case 600: return "Server busy";
		case 601: return "Server unavailable";
		case 602: return "Peer NS down";
		case 603: return "DB connection error";
		case 604: return "Service is going down";
		case 707: return "Error creating connection";
		case 711: return "Error with blocking write";
		case 712: return "Session overloaded";
		case 713: return "User too active";
		case 714: return "Too many sessions";
		case 715: return "Not expected";
		case 717: return "Bad friend file";
		case 800: return "Too many requests: slow down...";
		case 911: return "Authentication failed";
		case 913: return "Not allowed while Invisible";
		case 920: return "Not accepting new users";
		case 928: return "TWN authentication ticket rejected - sign-in failed.  This is probably a bug.";
			
	}

	return "Unknown error message reported by server";

}

void error_report_server(int number) {

	const char *error_message;
	
	error_message = decode_error(number);
	if ( strcmp(error_message, "IGNORE") != 0 ) {
		error_report(error_message);
	}

}

void error_message(const char *message) {
 
	GtkWidget *window;
	
	window = gtk_message_dialog_new(mainwindow_gtkwindow(), GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, message);
				
	g_signal_connect_swapped(window, "response", G_CALLBACK(gtk_widget_destroy), window);
	gtk_widget_show(window);

}
