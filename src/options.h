/*
 * options.h
 *
 * Low-level option handling
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */
 
#ifndef OPTIONS_H
#define OPTIONS_H

#include <gdk/gdk.h>

extern void options_load();
extern void options_save();

/* Single-function interfaces to option values.  Returned pointers must not be freed nor strings altered. */
extern const char *options_username();
extern const char *options_password();
extern const char *options_hostname();
extern unsigned short int options_port();
extern const char *options_wget();
extern const char *options_gtc();
extern const char *options_blp();
extern unsigned int options_rememberlogindetails();
extern unsigned int options_hideoffline();
extern unsigned int options_hidecsm();
extern const char *options_localcolour_string();
extern const GdkColor *options_localcolour_gdk();
extern unsigned int options_ofontoverride(void);
extern const GdkColor *options_ocolour_gdk();
extern const char *options_localfont();
extern const char *options_ofont();
extern unsigned int options_ircstyle();
extern unsigned int options_timestamps();
extern unsigned int options_showavatars();
extern unsigned int options_showemoticons();

extern void options_setusername(const char *username);
extern void options_setpassword(const char *password);
extern void options_sethostname(const char *hostname);
extern void options_setport(const int port);
extern void options_setwget(const char *wget);
extern void options_setrememberlogindetails(unsigned int remember);
extern void options_sethideoffline(unsigned int hide);
extern void options_sethidecsm(unsigned int hide);
extern void options_setlocalcolour_gdk(const GdkColor *colour);
extern void options_setofontoverride(unsigned int override);
extern void options_setocolour_gdk(const GdkColor *colour);
extern void options_setlocalfont(const char *font);
extern void options_setofont(const char *font);
extern void options_setircstyle(unsigned int ircstyle);
extern void options_settimestamps(unsigned int timestamps);
extern void options_setshowavatars(unsigned int showavatars);
extern void options_setshowemoticons(unsigned int showemoticons);

#endif	/* OPTIONS_H */
