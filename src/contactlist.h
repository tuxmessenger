/*
 * contactlist.h
 *
 * Contact list (FL, BL, AL and RL) data structures
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifndef CONTACTLIST_H
#define CONTACTLIST_H

#include <stdio.h>

#include "mainwindow.h"
#include "msngenerics.h"

/* Sources of contact details, in ascending order of accuracy */
typedef enum {
	CONTACT_SOURCE_FLN,	/* Details came from FLN (which provides no information) */
	CONTACT_SOURCE_LST,	/* Contact's details came from LST */
	CONTACT_SOURCE_CACHE,	/* Details came from the cache */
	CONTACT_SOURCE_ADC,	/* Details came from an ADC (asynchronous add) */
	CONTACT_SOURCE_RNG,	/* Details came from a RNG or JOI */
	CONTACT_SOURCE_NLN	/* Details came from NLN or ILN */
} ContactSource;

typedef unsigned int ContactListIter;

extern void contactlist_fldetails(ContactSource source, const char *username, const char *friendlyname, int features, const char *dpobject, OnlineState status, const char *guid);
extern void contactlist_aldetails(ContactSource source, const char *username, const char *friendlyname, OnlineState status);
extern void contactlist_bldetails(ContactSource source, const char *username, const char *friendlyname, OnlineState status);
extern void contactlist_rldetails(ContactSource source, const char *username, const char *friendlyname, OnlineState status);
extern void contactlist_tldetails(ContactSource source, const char *username, const char *friendlyname, OnlineState status);
extern void contactlist_pldetails(ContactSource source, const char *username, const char *friendlyname, OnlineState status);

extern void contactlist_removecontact(const char *list, const char *username);
extern void contactlist_removecontactguid(const char *list, const char *guid);

extern void contactlist_setubx(const char *username, const char *ubxdata, size_t ubxdatalen);
extern char *contactlist_csm(const char *username, int replace_entities);

extern void contactlist_clear();
extern int contactlist_isonlist(const char *list, const char *username);
extern const char *contactlist_friendlyname(const char *username);
extern const char *contactlist_haspicture(const char *username);
extern void contactlist_picturekick_sha1d(const char *sha1d);
extern const char *contactlist_dpsha1d(const char *username);

extern int contactlist_dumplist(FILE *fh, const char *list);
extern ContactListIter *contactlist_iter_new();
extern const char *contactlist_getusername(ContactListIter *iter, const char *list);
extern void contactlist_iter_destroy(ContactListIter *iter);

#endif	/* CONTACTLIST_H */
