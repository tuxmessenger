/*
 * msnp2p.h
 *
 * MSNP2P handling
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifndef MSNP2P_H
#define MSNP2P_H

#include "sbsessions.h"

extern void msnp2p_parsemsg(SbSession *session, const char *username, const char *msg, size_t len);
extern void msnp2p_getpicture(SbSession *session, const char *username, const char *dpobject);
extern void msnp2p_abortall(SbSession *session);
extern int msnp2p_retrieving(const char *dpsha1d);
extern void msnp2p_offerfile(SbSession *session, const char *username, const char *filename);

#endif	/* MSNP2P_H */
