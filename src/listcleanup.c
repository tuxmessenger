/*
 * listcleanup.c
 *
 * Automatic list cleanup feature - e.g. remove unnecessary blocks
 *
 * (c) 2002-2004 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "contactlist.h"
#include "debug.h"

void listcleanup_open() {

	ContactListIter *iter;
	const char *username = "";
	
	/* On FL but not RL (i.e. person doesn't like you) */
	iter = contactlist_iter_new();
	while ( username != NULL ) {
		username = contactlist_getusername(iter, "FL");
		if ( username != NULL ) {
			if ( !contactlist_isonlist("RL", username) ) {
				debug_print("LU: Hasn't added you: %s\n", username);
			}
		}
	}
	contactlist_iter_destroy(iter);

	/* On RL but not FL (i.e. you're being rude) */
	iter = contactlist_iter_new();
	username = "";
	while ( username != NULL ) {
		username = contactlist_getusername(iter, "RL");
		if ( username != NULL ) {
			if ( !contactlist_isonlist("FL", username) ) {
				debug_print("LU: You haven't added: %s\n", username);
			}
		}
	}
	contactlist_iter_destroy(iter);

	/* On AL but not RL (i.e. pointless Allow) */
	iter = contactlist_iter_new();
	username = "";
	while ( username != NULL ) {
		username = contactlist_getusername(iter, "AL");
		if ( username != NULL ) {
			if ( !contactlist_isonlist("RL", username) ) {
				debug_print("LU: Possible 'pointless allow': %s\n", username);
			}
		}
	}
	contactlist_iter_destroy(iter);

	/* On BL but not RL (i.e. pointless block) */
	iter = contactlist_iter_new();
	username = "";
	while ( username != NULL ) {
		username = contactlist_getusername(iter, "BL");
		if ( username != NULL ) {
			if ( !contactlist_isonlist("RL", username) ) {
				debug_print("LU: Possible 'pointless block': %s\n", username);
			}
		}
	}
	contactlist_iter_destroy(iter);

}
