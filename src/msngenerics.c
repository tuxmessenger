/*
 * msngenerics.c
 *
 * General MSN protocol schemas and semantics
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <assert.h>
#include <string.h>

#include "msngenerics.h"

OnlineState msngenerics_decodestatus(const char *status) {

	OnlineState state = ONLINE_ERR;

	if ( strcmp(status, "NLN") == 0 ) {
		state = ONLINE_NLN;
	}
	if ( strcmp(status, "IDL") == 0 ) {
		state = ONLINE_IDL;
	}
	if ( strcmp(status, "AWY") == 0 ) {
		state = ONLINE_AWY;
	}
	if ( strcmp(status, "BSY") == 0 ) {
		state = ONLINE_BSY;
	}
	if ( strcmp(status, "BRB") == 0 ) {
		state = ONLINE_BRB;
	}
	if ( strcmp(status, "PHN") == 0 ) {
		state = ONLINE_PHN;
	}
	if ( strcmp(status, "LUN") == 0 ) {
		state = ONLINE_LUN;
	}
	if ( strcmp(status, "HDN") == 0 ) {
		state = ONLINE_HDN;
	}
	if ( strcmp(status, "FLN") == 0 ) {
		state = ONLINE_FLN;
	}
	
	assert(state != ONLINE_ERR);
	
	/* It's up to the caller to check that the status given is actually valid in context. */
	return state;

}
