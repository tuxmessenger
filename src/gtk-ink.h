/*
 * gtk-ink.h
 *
 * GTK widgets to display and collect Ink
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifndef GTKINK_H
#define GTKINK_H

#include <gtk/gtk.h>
#include <libgnomecanvas/libgnomecanvas.h>

#include "ink.h"

typedef enum {
	GTK_INK_FLAG_EDITABLE	= 1<<0,		/* Editable by the user. */
	GTK_INK_FLAG_PAPER	= 1<<1,		/* Draw nice paper background. */
} GtkInkFlags;

typedef struct {
	GnomeCanvasClass parent_class;
} GtkInkClass;

typedef struct {

	GnomeCanvas parent;		/* Parent widget */

	Ink *ink;			/* Ink object this widget is associated with. */
	GtkInkFlags flags;		/* Flags for this GtkInk widget. */

	int width;			/* Width of drawing area. */
	int height;			/* Height of drawing area. */
	int drawing;			/* Currently drawing? */
	GdkColor *bgcolour;		/* Background colour for drawing area. */
	GdkColor *fgcolour;		/* Foreground colour for drawing area. */
	GdkPixbuf *bgpixbuf;		/* Pixbuf to use for the background. */
	
	int x_bg_drawn;
	int y_bg_drawn;			/* Number of background tiles drawn in the x and y directions. */

} GtkInk;

extern guint gtk_ink_get_type(void);
extern GtkWidget *gtk_ink_new(GtkInkFlags flags, Ink *ink, GdkColor *fgcolour, GdkColor *bgcolour);

#define GTK_INK(obj)		GTK_CHECK_CAST(obj, gtk_ink_get_type(), GtkInk)
#define GTK_INK_CLASS(class)	GTK_CHECK_CLASS_CAST(class, gtk_ink_get_type(), GtkInkClass)
#define GTK_IS_INK(obj)		GTK_CHECK_TYPE(obj, gtk_ink_get_type())

#endif	/* GTKINK_H */
