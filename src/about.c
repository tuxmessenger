/*
 * about.c
 *
 * The "About TuxMessenger" box
 *
 * (c) 2002-2007 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>

#include "debug.h"

void about_open() {

#ifdef HAVE_GTK_2_6_0
	GtkWidget *window;
	const gchar *authors[] = {
		"Thomas White <taw27@srcf.ucam.org>",
		"",
		"Thanks to:",
		"Andrew Millar",
		"Thomas Arbesser-Rastburg",
		"Ben Whitehead",
		"Marielle Vonk",
		"Siebe Tolsma",
		"Kevin Lu",
		"Matthew Mayer",
		"Neill Horie",
		"Barnaby Gray",
		"Hiroyuki Yamamoto",		
		NULL
	};
	
	window = gtk_about_dialog_new();
	
	gtk_about_dialog_set_name(GTK_ABOUT_DIALOG(window), PACKAGE_NAME);
	gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(window), PACKAGE_VERSION);
	gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(window), "(c) 2002-2007 Thomas White <taw27@srcf.ucam.org>");
	gtk_about_dialog_set_comments(GTK_ABOUT_DIALOG(window), "MSN Messenger Client");
	gtk_about_dialog_set_license(GTK_ABOUT_DIALOG(window), "This package is free software; you can redistribute it and/or modify\n"
								"it under the terms of the GNU General Public License as published by\n"
								"the Free Software Foundation; version 2 dated June, 1991.\n\n"
								"This package is distributed in the hope that it will be useful,\n"
								"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
								"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
								"GNU General Public License for more details.\n\n"
								"You should have received a copy of the GNU General Public License\n"
								"along with this package; if not, write to the Free Software\n"
								"Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA\n"
								"02111-1307, USA.\n");
	gtk_about_dialog_set_website(GTK_ABOUT_DIALOG(window), "http://www.srcf.ucam.org/~taw27/");
	gtk_about_dialog_set_authors(GTK_ABOUT_DIALOG(window), authors);
	
	g_signal_connect(window, "response", G_CALLBACK(gtk_widget_destroy), NULL);
	
	gtk_widget_show_all(window);
#else /* HAVE_GTK_2_6_0 */
	debug_print("AB: About window doesn't work without GTK 2.6.0 :(\n");
#endif /* HAVE_GTK_2_6_0 */

}
