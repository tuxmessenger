/*
 * ink.c
 *
 * Routines for handling Ink
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "ink.h"
#include "assert.h"
#include "routines.h"

/* Create a new Ink object. */
Ink *ink_new() {

	Ink *ink = malloc(sizeof(Ink));
	
	ink->strokes = NULL;
	ink->last_stroke = NULL;

	return ink;

}

static Ink *ink_new_from_isf_base64(const char *base64_isf) {

	Ink *ink;
	size_t isf_len;
	char *isf;
	FILE *fh = fopen("/home/weiss/isf.dat", "w");

	ink = ink_new();
	isf_len = routines_base64decode(base64_isf, &isf);

	printf("'%s' (%i bytes)\n", isf, isf_len);
	fwrite(isf, isf_len, 1, fh);
	fclose(fh);

	return ink;

}

/* Create a new Ink object and load it with data from an ISF string. */
Ink *ink_new_from_isf(const char *isf) {

	if ( strncmp(isf, "base64:", 7) == 0 ) {
		return ink_new_from_isf_base64(isf+7);
	}

	/* Can't load anything else at the moment.  Return a blank. */
	return ink_new();

}

/* Create a new stroke in a given Ink object. */
InkStroke *ink_stroke_new(Ink *ink) {

	InkStroke *stroke = malloc(sizeof(InkStroke));
	
	if ( ink->last_stroke != NULL ) {
		assert(ink->last_stroke->next == NULL);
		ink->last_stroke->next = stroke;
	} else {
		/* No strokes */
		ink->strokes = stroke;
	}	
	
	ink->last_stroke = stroke;
	
	stroke->next = NULL;
	stroke->points = NULL;
	stroke->last_point = NULL;
	
	return stroke;
	
}

/* Get last stroke - automatically creating the first one if there aren't any. */
InkStroke *ink_stroke_get_last(Ink *ink) {

	if ( ink->last_stroke != NULL ) {
		return ink->last_stroke;
	}
	
	return ink_stroke_new(ink);	/* Automatically becomes the last stroke. */
	
}

InkPoint *ink_point_get_last(InkStroke *stroke) {

	assert(stroke != NULL);
	return stroke->last_point;	/* Might be NULL. */

}

InkPoint *ink_point_get_previous(InkPoint *point) {

	if ( point == NULL ) {
		return NULL;
	}
	
	return point->prev;	/* Might be NULL. */

}

/* Create a new point in a given stroke. */
InkPoint *ink_point_add(InkStroke *stroke, double x, double y) {

	InkPoint *last_point = ink_point_get_last(stroke);
	InkPoint *point = malloc(sizeof(InkPoint));
	
	if ( last_point != NULL ) {
		assert(last_point->next == NULL);
		last_point->next = point;
		point->prev = last_point;
	} else {
		/* No points! */
		stroke->points = point;
		point->prev = NULL;
	}
	
	stroke->last_point = point;
	
	point->next = NULL;
	point->x = x;
	point->y = y;
	
	return point;
	
}

/* Destroy an Ink object. */
void ink_destroy(Ink *ink) {
	free(ink);
}
