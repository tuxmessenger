/*
 * statusicons.c
 *
 * Little pixmaps to go next to the contacts in the list
 *
 * (c) 2002-2004 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <assert.h>

#include "debug.h"
#include "mainwindow.h"

static struct {

	GdkPixmap *online_pixmap;
	GdkPixmap *offline_pixmap;
	GdkPixmap *away_pixmap;
	GdkPixmap *busy_pixmap;
	
	GdkPixmap *online_blocked_pixmap;
	GdkPixmap *offline_blocked_pixmap;
	GdkPixmap *away_blocked_pixmap;
	GdkPixmap *busy_blocked_pixmap;

} statusicons = {

	NULL,
	NULL,
	NULL,
	NULL,
	
	NULL,
	NULL,
	NULL,
	NULL

};

/* Return the correct pixmap to pack into the contact's widget */
GtkWidget *statusicons_pixmap(OnlineState status) {

	GdkPixmap **pixmap;
	char *file;
	
	pixmap = NULL;
	file = NULL;

	if ( status == ONLINE_NLN ) {
		pixmap = &statusicons.online_pixmap;
		file = DATADIR"/tuxmessenger/online.xpm";
	} else if ( status == ONLINE_IDL ) {
		pixmap = &statusicons.away_pixmap;
		file = DATADIR"/tuxmessenger/away.xpm";
	} else if ( status == ONLINE_AWY ) {
		pixmap = &statusicons.away_pixmap;
		file = DATADIR"/tuxmessenger/away.xpm";
	} else if ( status == ONLINE_BSY ) {
		pixmap = &statusicons.busy_pixmap;
		file = DATADIR"/tuxmessenger/occ.xpm";
	} else if ( status == ONLINE_LUN ) {
		pixmap = &statusicons.away_pixmap;
		file = DATADIR"/tuxmessenger/away.xpm";
	} else if ( status == ONLINE_BRB ) {
		pixmap = &statusicons.away_pixmap;
		file = DATADIR"/tuxmessenger/away.xpm";
	} else if ( status == ONLINE_PHN ) {
		pixmap = &statusicons.busy_pixmap;
		file = DATADIR"/tuxmessenger/occ.xpm";
	} else if ( status == ONLINE_FLN ) {
		pixmap = &statusicons.offline_pixmap;
		file = DATADIR"/tuxmessenger/offline.xpm";
	}
	
	assert(pixmap != NULL);
	assert(file != NULL);
	
	if ( *pixmap == NULL ) {
	
		/* XPM hasn't been loaded yet. Load it. */
		GtkStyle *style;
		style = mainwindow_style();
		*pixmap = gdk_pixmap_create_from_xpm(mainwindow_window(), NULL, &style->bg[GTK_STATE_NORMAL], file);
		if ( *pixmap == NULL ) {
			/* Contingency if the image can't be loaded. */
			debug_print("SI: Couldn't load status icon!\n");
			return gtk_label_new("");
		}
		
	}
	
	return gtk_pixmap_new(*pixmap, NULL);
	
}
