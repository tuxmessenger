/*
 * fonttrans.c
 *
 * Translate to and from MSN-style format strings
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 *  This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "routines.h"
#include "debug.h"

char *fonttrans_font_to_format(const char *font) {

	char *format;
	char *temp;
	int family = 0;
	int pitch = 0;
	char *fontname;
	int i;
	
	debug_print("FO: Font '%s'\n", font);
	
	format = malloc(128);
	
	/* Turn the font name into something useful at the other end. */
	strcpy(format, "FN=");
	fontname = strdup(font);
	
	/* Cut off the font size */
	for ( i=strlen(fontname); i>0; i-- ) {
		if ( fontname[i] == ' ' ) {
			fontname[i] = '\0';
			break;
		}
	}
	
	/* Cut out "Bold", "Italic" etc. */
	if ( (temp = strstr(fontname, " Bold")) ) {
		strcpy(temp, temp+5);
	}
	if ( (temp = strstr(fontname, " Oblique")) ) {
		strcpy(temp, temp+8);
	}
	if ( (temp = strstr(fontname, " Italic")) ) {
		strcpy(temp, temp+7);
	}
	if ( (temp = strstr(fontname, " Underline")) ) {
		strcpy(temp, temp+10);
	}
	if ( (temp = strstr(fontname, " Strikethrough")) ) {
		strcpy(temp, temp+14);
	}
	
	temp = routines_urlencode(fontname);
	free(fontname);
	strncat(format, temp, 100);
	free(temp);
	
	/* Work out bold, underline, italic etc. */
	strcat(format, "; EF=");
	if ( strstr(font, " Bold ") ) {
		strcat(format, "B");
	}
	if ( strstr(font, " Italic ") || strstr(font, " Oblique ") ) {
		strcat(format, "I");
	}
	if ( strstr(font, " Strikethrough ") ) {
		strcat(format, "S");
	}
	if ( strstr(font, " Underline ") ) {
		strcat(format, "U");
	}
	
	/* Work out font family and pitch. */
	strcat(format, "; PF=");
	if ( strstr(font, "Serif") || strstr(font, "Palladio") || strstr(font, "Bookman") || strstr(font, "Roman") ) {
		family = 1;
	}
	if ( strstr(font, "Sans") || strstr(font, "Gothic") ) {
		family = 2;
	}
	if ( strstr(font, "Monospace") || strstr(font, "Mono ") || strstr(font, "System") || strstr(font, "Fixed") ) {
		family = 3;
	}
	if ( strstr(font, "Script") || strstr(font, "Chancery") ) {
		family = 4;
	}
	if ( strstr(font, "Comic") ) {
		family = 5;
	}
	
	/* Pitch */
	if ( strstr(font, "Mono ") || strstr(font, "Monospace") || strstr(font, "Fixed") || strstr(font, "System") ) {
		pitch = 1;
	} else {
		pitch = 2;
	}
	
	temp = malloc(3);
	snprintf(temp, 3, "%i%i", family, pitch);
	strcat(format, temp);
	free(temp);
	
	debug_print("FO: Format '%s'\n", format);
	
	return format;

}
