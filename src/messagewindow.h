/*
 * messagewindow.h
 * 
 * IM windows
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifndef MESSAGEWINDOW_H
#define MESSAGEWINDOW_H

#include "sbsessions.h"
#include "gtk-ink.h"

typedef struct _mwuser {

	struct _mwuser *next;
	
	char *username;

	GtkWidget *bar;			/* GTK label containing the user's name. */
	GtkWidget *bar_hbox;		/* hbox into which "bar" fits */
	GtkWidget *avatar_eventbox;	/* Eventbox into which the "Display picture" fits */
	GtkWidget *avatar;		/* The "Display picture" itself (GtkImage) */
	GdkPixbuf *avatar_pixbuf;	/* Corresponding pixbuf */
	
	int typing_callback;		/* Callback to unset "(Typing) " status. */
	
} MwUser;

typedef struct _messagewindow {

	struct _messagewindow *next;
	
	SbSession *session;		/* SB session this window talks to */

	MwUser *users;
	unsigned int num_users;		/* Number of people in this window. */
	unsigned int first_event;
	unsigned int in_creation;	/* Non-zero suppresses opening of an SB session. */
	unsigned int last_was_nak;	/* Last text to this window was NAK warning. */

	GtkWidget *window;		/* Overall window. */
	GtkWidget *windowbox;		/* VBox into which to pack Stuff (like status bars) */
	GtkWidget *bigvbox;
	GtkWidget *avatar_eventbox;	/* Eventbox into which the "Display picture" fits */
	GtkWidget *avatar;		/* The "Display picture" itself (GtkImage) */
	GdkPixbuf *avatar_pixbuf;	/* Corresponding pixbug */
	GtkWidget *picture_list;	/* VBox into which the column of avatars fits */
	GtkWidget *messages;		/* Widget into which to put messages */
	GtkWidget *textbox;		/* Text Entry widget */
	GtkTextMark *mark;		/* Mark at the end of the text. */
	GtkUIManager *ui;		/* UI manager */
	GtkActionGroup *action_group;	/* Action group */
	GtkWidget *fontsdialog;		/* Fonts dialog box */
	GtkWidget *ofont_button;	/* Font button to set override font. */
	GtkWidget *ocolour_button;	/* Colour button to set override font. */
	GtkWidget *gtk_ink;		/* GtkInk widget */
	GtkWidget *textbox_hbox;
	GtkWidget *ink_button;
	unsigned int stuck;		/* Flag to see if the window is scrolled to the bottom. */

	GdkColor *localcolour_gdk;	/* GdkColor version of local user's colour. */
	char *localcolour_string;	/* String version of local user's colour. */
	GdkColor *ocolour_gdk;	/* GdkColor to override contacts' colours with. */
	unsigned int ofontoverride;		/* Override contacts' colours? */
	char *ocolour_string;		/* String version of ocolour_gdk */
	char *localfont;		/* Local user's font in a format to throw at the SB (FN=xxx EF=xxx etc) */
	char *dislocalfont;		/* Local user's font in a format to use for local display. */
	char *ofont;			/* Font for contacts' messages. */

	unsigned int ircstyle;
	unsigned int timestamps;
	unsigned int showemoticons;

} MessageWindow;

/* Operations on IM windows themselves. */
extern MessageWindow *messagewindow_create(const char *username, SbSession *session);
extern void messagewindow_create_if_none(const char *username, SbSession *session);
extern void messagewindow_mitigate(SbSession *session);
extern void messagewindow_unplug(SbSession *session);
extern void messagewindow_addtext_system(MessageWindow *messagewindow, const char *text);
extern void messagewindow_addtext_user_nonewline(MessageWindow *messagewindow, const char *text, int length, const char *colour, const char *font);
extern void messagewindow_reportdropped(MessageWindow *messagewindow, char *message, size_t length);
extern int messagewindow_get_last_was_nak(MessageWindow *messagewindow);
extern void messagewindow_set_last_was_nak(MessageWindow *messagewindow, int last_was_nak);

/* Operations on individual IM window users. */
extern void messagewindow_joined(MessageWindow *messagewindow, const char *username);
extern void messagewindow_removeuser(MessageWindow *messagewindow, const char *username);
extern void messagewindow_starttyping(MessageWindow *messagewindow, const char *username);
extern int messagewindow_stoptyping(MwUser *user);
extern void messagewindow_stoptypingbyusername(MessageWindow *messagewindow, const char *username);
extern void messagewindow_picturekick(const char *username);
extern void messagewindow_notifyoffline(const char *username);

/* Other stuff */
extern void messagewindow_disable_all();
extern void messagewindow_enable_all();

#endif	/* MESSAGEWINDOW_H */
