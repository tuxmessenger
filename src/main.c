/*
 * main.c
 *
 * The Top Level Source File
 *
 * (c) 2002-2004 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 *  This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdarg.h>
#include <signal.h>

#include "mainwindow.h"
#include "options.h"
#include "debug.h"

int main (int argc, char *argv[]) {

	/* This makes it look so easy... */
	gtk_init(&argc, &argv);
	gtk_window_set_default_icon_from_file(DATADIR"/tuxmessenger/icon.png", NULL);
	
	options_load();
	mainwindow_open();
	/* Prevent horrid sudden death */
	signal(SIGPIPE, SIG_IGN);
	gtk_main();
	
	return 0;
	
}

char *tuxmessenger_versionstring() {

#ifdef HAVE_CONFIG_H
	return PACKAGE_NAME"/"VERSION;
#else
	return "TuxMessenger/unknown";
#endif

}
