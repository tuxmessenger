/*
 * avatars.h
 *
 * Handling of User Display Pictures (but not displaying them)
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifndef AVATARS_H
#define AVATARS_H

extern char *avatars_havepicture(const char *username);
extern char *avatars_default_none();
extern char *avatars_default_fetching();
extern char *avatars_local();
extern char *avatars_localobject();
extern char *avatars_unwrapsha1d(const char *dpobject);
extern char *avatars_getlocalfilename(const char *name);

#endif	/* AVATARS_H */
