/*
 * twnauth.c
 *
 * TWN authentication
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "debug.h"
#include "options.h"
#include "routines.h"

/* Nothing insecure happens if the server data won't fit in this - anything
	beyond this limit simply gets ignored. */
#define TWNAUTH_WGETSIZE 4096
//#define TWNAUTH_DEBUG 1

/* Internally called to get the login URL from Nexus */
char *twnauth_loginurl() {

	char *shellcommand;
	int read_more;
	FILE *wget_stream;
	char *nexus = NULL;
	char *wget_buffer;
	int wget_offset = 0;
	size_t i;
			
	shellcommand = malloc(1024);
	strncpy(shellcommand, options_wget(), 256);
	shellcommand[255] = '\0';
	strcat(shellcommand, " -S -O - https://nexus.passport.com/rdr/pprdr.asp 2>&1");
	
	debug_print("twnauth: nexus: %s\n", shellcommand);
	wget_stream = popen(shellcommand, "r");
	
	if ( wget_stream == NULL ) {
		debug_print("twnauth: Nexus failed.\n");
		return strdup("");
	}

	wget_buffer = malloc(TWNAUTH_WGETSIZE);
	read_more = 1;
	while ( read_more ) {

		ssize_t readval;
		readval = fread(wget_buffer + wget_offset, 1, TWNAUTH_WGETSIZE-wget_offset, wget_stream);
		if ( (readval != 0) && (readval != -1) && (wget_offset < TWNAUTH_WGETSIZE)  ) {
			wget_offset += readval;
		} else {
			read_more = 0;
			wget_buffer[wget_offset] = '\0';
		}

	}
	
	pclose(wget_stream);

#ifdef TWNAUTH_DEBUG
	debug_print("'%s'\n", wget_buffer);
#endif
	
	/* Now try and find the new nexus in the wget output */
	for ( i=0; i<strlen(wget_buffer); i++ ) {

		if ( strncmp((wget_buffer+i), "DALogin=", 8) == 0 ) {
				
			size_t j;

			/* Find the end of the ticket and tie it off */
			for ( j=i+7; j<strlen(wget_buffer); j++ ) {
				if ( wget_buffer[j] == ',' ) {
					wget_buffer[j] = '\0';
				}
			}
					
			nexus = strdup(wget_buffer+i+8);
			debug_print("twnauth: nexus: %s\n", nexus);
					
		}
		
	}
	
	free(wget_buffer);
	free(shellcommand);

	if ( nexus == NULL ) {
		debug_print("Warning! Couldn't retrieve URL from Nexus!\n");
		nexus = strdup("");
	}
	return nexus;
	
}

char *twnauth_ticket(char *authdata) {

	char *shellcommand;
	int read_more;
	FILE *wget_stream;
	char *options_username_2;
	char *auth_ticket = NULL;
	char *wget_buffer;
	int wget_offset = 0;
	size_t i;
	char *loginurl;
			
	assert(strlen(authdata) < 255);
	
	loginurl = twnauth_loginurl();

	shellcommand = malloc(1024);
	strncpy(shellcommand, options_wget(), 256);
	shellcommand[255] = '\0';
	strcat(shellcommand, " -S -O - --header='Authorization: Passport1.4 OrgVerb=GET,OrgURL=http%3A%2F%2Fmessenger%2Emsn%2Ecom,sign-in=");
	options_username_2 = routines_urlencode(options_username());
	strncat(shellcommand, options_username_2, 64);
	shellcommand[1023] = '\0';
	free(options_username_2);
	strncat(shellcommand, ",pwd=", 64);
	shellcommand[1023] = '\0';
	strncat(shellcommand, options_password(), 64);
	shellcommand[1023] = '\0';
	strcat(shellcommand, ",");
	shellcommand[1023] = '\0';
	strncat(shellcommand, authdata, 256);
	shellcommand[1023] = '\0';
	strcat(shellcommand, "' https://");
	shellcommand[1023] = '\0';
	strncat(shellcommand, loginurl, 64);
	shellcommand[1023] = '\0';
	free(loginurl);
	strcat(shellcommand, " 2>&1");
	shellcommand[1023] = '\0';
	
#ifdef TWNAUTH_DEBUG
	debug_print("twnauth: %s\n", shellcommand);
#else
	debug_print("twnauth: (sending authentication data)\n");
#endif
	wget_stream = popen(shellcommand, "r");
	
	if ( wget_stream == NULL ) {
		return NULL;
	}

	wget_buffer = malloc(TWNAUTH_WGETSIZE);
	read_more = 1;
	while ( read_more ) {

		ssize_t readval;
		readval = fread(wget_buffer + wget_offset, 1, TWNAUTH_WGETSIZE-wget_offset, wget_stream);
		if ( (readval != 0) && (readval != -1) && (wget_offset < TWNAUTH_WGETSIZE) ) {
			wget_offset += readval;
		} else {
			read_more = 0;
			wget_buffer[wget_offset] = '\0';
		}

	}
	
	pclose(wget_stream);

#ifdef TWNAUTH_DEBUG
	debug_print("'%s'\n", wget_buffer);
#endif
	
	/* Now try and find the ticket in the wget output */
	for ( i=0; i<strlen(wget_buffer); i++ ) {

		if ( strncmp((wget_buffer+i), "from-PP='t=:", 11) == 0 ) {
				
			size_t j;

			/* Find the end of the ticket and tie it off */
			for ( j=i+10; j<strlen(wget_buffer); j++ ) {
				if ( wget_buffer[j] == '\'' ) {
					wget_buffer[j] = '\0';
				}
			}
					
			auth_ticket = strdup(wget_buffer+i+9);
			debug_print("twnauth: %s\n", auth_ticket);
					
		}
		
	}
	
	free(wget_buffer);
	free(shellcommand);

	return auth_ticket;
	
}
