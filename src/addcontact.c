/*
 * addcontact.c
 *
 * UI parts of adding new contacts, and being added yourself.
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "mainwindow.h"
#include "debug.h"
#include "error.h"
#include "msnprotocol.h"
#include "routines.h"
#include "contactlist.h"

typedef struct stru_addedwindow {

	struct stru_addedwindow *next;
	GtkWidget *window;
	char *username;
	GtkWidget *allow_radio;
	GtkWidget *block_radio;
	GtkWidget *forward_toggle;
	
} AddedWindow;

typedef struct {

	GtkWidget *window;
	GtkWidget *allow_toggle;
	GtkWidget *username_entry;

} AddContactWindow;

static AddedWindow *addedwindows_list = NULL;

static int addcontact_destroyed(GtkWidget *widget, AddContactWindow *item) {
	free(item);
	return 0;
}

static int addcontact_response(GtkWidget *widget, gint response, AddContactWindow *item) {

	const char *username;

	if ( response == GTK_RESPONSE_REJECT ) {
		return 0;
	}

	if ( msnprotocol_signedin() ) {
		
		username = gtk_entry_get_text(GTK_ENTRY(item->username_entry));
		debug_print("AC: Adding new contact: '%s'\n", username);
		
		if ( strstr(username, "@") == NULL ) {
			/* Err... nope. */
			error_report("Invalid Passport address.");
			return 0;
		}
	
		msnprotocol_adduserfriendly(username, username, "FL");
		if ( gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(item->allow_toggle)) ) {
			msnprotocol_adduser(username, "AL");
		}

	} else {
	
		error_report("Try again when you're signed in.");
	
	}

	return 0;

}

static int addcontact_activate(GtkWidget *widget, AddContactWindow *item) {

	addcontact_response(item->window, GTK_RESPONSE_ACCEPT, item);
	gtk_widget_destroy(item->window);

	return 0;
	
}

void addcontact_open() {

	GtkWidget *hbox;
	GtkWidget *vbox;
	GtkWidget *icon;
	AddContactWindow *item;
	
	item = malloc(sizeof(AddContactWindow));

	item->window = gtk_dialog_new_with_buttons("Add New Contact", mainwindow_gtkwindow(), GTK_DIALOG_DESTROY_WITH_PARENT,
						GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
						GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
						NULL);
	
	g_signal_connect(item->window, "response", G_CALLBACK(addcontact_response), item);
	g_signal_connect_swapped(item->window, "response", G_CALLBACK(gtk_widget_destroy), item->window);
	g_signal_connect(item->window, "destroy", G_CALLBACK(addcontact_destroyed), item);
	
	hbox = gtk_hbox_new(FALSE, 20);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(item->window)->vbox), hbox);
	icon = gtk_image_new_from_stock(GTK_STOCK_ADD, GTK_ICON_SIZE_DIALOG);
	gtk_box_pack_start(GTK_BOX(hbox), icon, TRUE, TRUE, 0);
	
	vbox = gtk_vbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(hbox), vbox, TRUE, TRUE, 0);
	
	gtk_box_pack_start(GTK_BOX(vbox), gtk_label_new("Enter the Passport address of your new contact:"), TRUE, TRUE, 0);

	item->username_entry = gtk_entry_new();
	gtk_box_pack_start(GTK_BOX(vbox), item->username_entry, TRUE, TRUE, 0);
	g_signal_connect(item->username_entry, "activate", G_CALLBACK(addcontact_activate), item);

	item->allow_toggle = gtk_check_button_new_with_label("Allow this new contact to message you.");
	gtk_box_pack_start(GTK_BOX(vbox), item->allow_toggle, TRUE, TRUE, 0);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(item->allow_toggle), TRUE);
		
	gtk_container_set_border_width(GTK_CONTAINER(hbox), 10);
	gtk_widget_show_all(item->window);

}

static int addcontact_addedclose(GtkWidget *widget, gpointer data) {

	AddedWindow *addedwindow = (AddedWindow *)data;
	AddedWindow *previous = NULL;
	AddedWindow *check = addedwindows_list;

	debug_print("AC: Removing New Contact window for %s\n", addedwindow->username);

	/* Remove from list. */
	while ( check != NULL ) {
		if ( check == addedwindow ) {
			free(check->username);
			if ( previous != NULL ) {
				previous->next = check->next;
			} else {
				/* Was first on the list */
				addedwindows_list = check->next;
			}
			free(check);
			return 0;
		}
		previous = check;
		check = check->next;
	}
	
	return 0;

}

static int addcontact_addedresponse(GtkWidget *widget, gint response, gpointer data) {

	AddedWindow *addedwindow = (AddedWindow *)data;

	if ( response == GTK_RESPONSE_REJECT ) {
		addcontact_addedclose(widget, data);
		return 0;
	}
	
	if ( gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(addedwindow->forward_toggle)) ) {
	 	/* Add to FL.  Friendlyname isn't given: it'll arrive soon on NLN. */
		msnprotocol_adduserfriendly(addedwindow->username, addedwindow->username, "FL");
	}

	if ( gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(addedwindow->allow_radio)) ) {
		/* Add to AL */
		msnprotocol_adduser(addedwindow->username, "AL");
	} else {
		/* Add to BL */
		msnprotocol_adduser(addedwindow->username, "BL");
	}
	
	/* Remove from PL and add to RL */
	msnprotocol_adduser(addedwindow->username, "RL");
	msnprotocol_remuser(addedwindow->username, "PL");
	
	addcontact_addedclose(widget, data);
	
	return 0;

}

void addcontact_added(const char *username, const char *friendlyname_coded) {

	GtkWidget *label;
	char *text;
	AddedWindow *addedwindow;
	GtkWidget *hbox;
	GtkWidget *vbox;
	GtkWidget *icon;
	char *friendlyname;
	
	debug_print("AC: New contact! %s / %s\n", username, friendlyname_coded);

	addedwindow = malloc(sizeof(AddedWindow));
	addedwindow->next = addedwindows_list;
	debug_print("AC: Next = %p\n", addedwindow->next);
	addedwindow->username = strdup(username);
	addedwindows_list = addedwindow;
	
	if ( friendlyname_coded != NULL ) {
		friendlyname = routines_urldecode(friendlyname_coded);
	} else {
		friendlyname = NULL;
	}
	
	if ( friendlyname != NULL ) {
		text = malloc(45 + strlen(username) + strlen(friendlyname));
		sprintf(text, "'%s' (%s) added you to his/her contact list.", friendlyname, username);
		free(friendlyname);
	} else {
		text = malloc(40 + strlen(username));
		sprintf(text, "'%s' added you to his/her contact list.", username);
	}

	addedwindow->window = gtk_dialog_new_with_buttons("New Contact", mainwindow_gtkwindow(), GTK_DIALOG_DESTROY_WITH_PARENT,
						GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
						GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
						NULL);
	
	g_signal_connect(addedwindow->window, "response", G_CALLBACK(addcontact_addedresponse), addedwindow);
	g_signal_connect_swapped(addedwindow->window, "response", G_CALLBACK(gtk_widget_destroy), addedwindow->window);
	g_signal_connect(addedwindow->window, "delete_event", G_CALLBACK(addcontact_addedclose), addedwindow);
	
	hbox = gtk_hbox_new(FALSE, 20);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(addedwindow->window)->vbox), hbox);
	icon = gtk_image_new_from_stock(GTK_STOCK_DIALOG_INFO, GTK_ICON_SIZE_DIALOG);
	gtk_box_pack_start(GTK_BOX(hbox), icon, TRUE, TRUE, 0);
	
	vbox = gtk_vbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(hbox), vbox, TRUE, TRUE, 0);
	
	label = gtk_label_new(text);
	gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0);
	addedwindow->allow_radio = gtk_radio_button_new_with_label(NULL, "Add him/her to your Allow List.");
	addedwindow->block_radio = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(addedwindow->allow_radio), "Block him/her.");
	addedwindow->forward_toggle = gtk_check_button_new_with_label("Add him/her to your contact list as well.");
	gtk_box_pack_start(GTK_BOX(vbox), addedwindow->allow_radio, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), addedwindow->block_radio, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), addedwindow->forward_toggle, TRUE, TRUE, 0);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(addedwindow->forward_toggle), TRUE);
	
	/* Disable "Add to your list as well" option if they're already on the FL. */
	gtk_widget_set_sensitive(GTK_WIDGET(addedwindow->forward_toggle), !contactlist_isonlist("FL", username));
		
	gtk_box_pack_start(GTK_BOX(vbox), gtk_label_new("If you press Cancel, you will be asked again the next time you sign in."), TRUE, TRUE, 0);
	gtk_container_set_border_width(GTK_CONTAINER(hbox), 10);
	
	gtk_widget_show_all(addedwindow->window);
	free(text);
	
}

void addcontact_closeall() {

	AddedWindow *item = addedwindows_list;
	
	while ( item != NULL ) {
	
		AddedWindow *next_item;
		
		next_item = item->next;
		gtk_widget_destroy(item->window);
		debug_print("AC: Next = %p\n", next_item);
		addcontact_addedclose(NULL, item);
		item = next_item;
		
	}

}
