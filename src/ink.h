/*
 * ink.h
 *
 * Routines for handling Ink
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifndef INK_H
#define INK_H

typedef struct stru_inkpoint {

	struct stru_inkpoint *next;	/* Next point in linked list */
	
	double x;
	double y;
	
	struct stru_inkpoint *prev;	/* Previous point in linked list */
	
} InkPoint;

typedef struct stru_inkstroke {

	struct stru_inkstroke *next;		/* Next stroke in linked list */
	InkPoint *points;			/* Linked list of points. */
	
	InkPoint *last_point;		/* Last point in list (saves rescanning) */
	
} InkStroke;

/* Opaque! */
typedef struct {

	InkStroke *strokes;			/* Linked list of strokes */
	
	InkStroke *last_stroke;			/* Last stroke in list (saves rescanning) */
	
} Ink;

extern Ink *ink_new();
extern Ink *ink_new_from_isf(const char *isf);
extern void ink_destroy(Ink *ink);

extern InkStroke *ink_stroke_new(Ink *ink);
extern InkStroke *ink_stroke_get_last(Ink *ink);

extern InkPoint *ink_point_add(InkStroke *stroke, double x, double y);
extern InkPoint *ink_point_get_last(InkStroke *stroke);
extern InkPoint *ink_point_get_previous(InkPoint *point);

#endif	/* INK_H */
