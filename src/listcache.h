/*
 * listcache.h
 * 
 * Contact list caching
 *
 * (c) 2002-2005 Thomas White <taw27@srcf.ucam.org>
 *  Part of TuxMessenger - GTK+-based MSN Messenger client
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991. 
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 *
 */

#ifndef LISTCACHE_H
#define LISTCACHE_H

extern char *listcache_load(void);
extern char *listcache_loadtag(void);
extern void listcache_save(void);
extern void listcache_invalidate();

extern void listcache_settag(const char *tag);
extern void listcache_setmfn(const char *mfn);
extern const char *listcache_getmfn(void);

extern void listcache_setcsm(const char *csm);
extern char *listcache_getcsm(void);
extern char *listcache_getcsm_noentities(void);

#endif 	/* LISTCACHE_H */
